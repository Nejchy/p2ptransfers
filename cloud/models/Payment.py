from google.appengine.ext import ndb


class Payment(ndb.Model):
    payment_confirmed = ndb.BooleanProperty(required=True)
    payment_made = ndb.BooleanProperty(required=True)
    distance_travelled = ndb.FloatProperty()
    amount = ndb.FloatProperty(required=True)
    payment_date = ndb.DateTimeProperty(auto_now_add=True)
    receive_address = ndb.StringProperty()


def GetPaymentByID(payment_id):
    return Payment.get_by_id(payment_id)