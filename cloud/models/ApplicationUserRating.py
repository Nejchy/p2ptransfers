from google.appengine.ext import ndb


class ApplicationUserRating(ndb.Model):
    transfer_request_id = ndb.IntegerProperty(required=True)
    rating = ndb.IntegerProperty(required=True)
    rating_user_id = ndb.StringProperty(required=True)
    voting_user_id = ndb.StringProperty(required=True)
    rating_datetime = ndb.DateTimeProperty(auto_now=True)
    is_rating_for_passenger = ndb.BooleanProperty()


def CalculateDriverRating(rating_user_id):
    number_of_ratings = 0
    sum_of_ratings = 0
    query = ApplicationUserRating.query(ApplicationUserRating.rating_user_id == rating_user_id,
                                        ApplicationUserRating.is_rating_for_passenger == False)

    for rating in query:
        number_of_ratings += 1
        sum_of_ratings += rating.rating

    if number_of_ratings == 0:
        return 0

    return sum_of_ratings / number_of_ratings


def CalculatePassengerRating(rating_user_id):
    number_of_ratings = 0
    sum_of_ratings = 0
    query = ApplicationUserRating.query(ApplicationUserRating.rating_user_id == rating_user_id,
                                        ApplicationUserRating.is_rating_for_passenger == True)

    for rating in query:
        number_of_ratings += 1
        sum_of_ratings += rating.rating

    if number_of_ratings == 0:
        return 0

    return sum_of_ratings / number_of_ratings


def GetRatingForRatedUserIfExists(transfer_request_id, rated_application_user_id):
    return ApplicationUserRating.query(
        ApplicationUserRating.transfer_request_id == transfer_request_id,
        ApplicationUserRating.rating_user_id == rated_application_user_id).get()


def GetRatingForVoteingUserIfExists(transfer_request_id, voting_application_user_id):
    return ApplicationUserRating.query(
        ApplicationUserRating.transfer_request_id == transfer_request_id,
        ApplicationUserRating.voting_user_id == voting_application_user_id).get()
