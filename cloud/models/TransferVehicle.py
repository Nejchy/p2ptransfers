__author__ = 'Nejc'

from google.appengine.ext import db


class TransferVehicle(db.Model):
    model = db.StringProperty(required=True)
    make = db.StringProperty(required=True)
    registration_number = db.StringProperty(required=True)
    color = db.StringProperty(required=True)
    number_of_passenger_seats = db.IntegerProperty(required=True)
    application_user_id = db.StringProperty(required=True)

    def getDescription(self):
        return self.make + ' ' + self.model + ' ' + self.registration_number


def GetTransferVehiclesForUser(application_user_id):
    q = db.GqlQuery("SELECT * FROM TransferVehicle " +
                    "WHERE application_user_id = :1 " +
                    "ORDER BY registration_number DESC",
                    application_user_id)
    return q.fetch(limit=None)


def GetTransferVehicleByID(transfer_vehicle_id):
    return TransferVehicle.get_by_id(transfer_vehicle_id)