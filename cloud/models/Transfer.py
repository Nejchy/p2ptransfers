from google.appengine.ext import ndb
from business_logic import Utils
from models import TransferVehicle, TransferRoute, ApplicationUser
from models.TransferStop import TransferStop


class Transfer(ndb.Model):
    transfer_vehicle = ndb.StringProperty(required=True)
    transfer_route = ndb.StringProperty(required=True)
    pricing_option = ndb.IntegerProperty(required=True)
    amount = ndb.FloatProperty(required=True)
    created = ndb.DateTimeProperty(auto_now_add=True)
    application_user_id = ndb.StringProperty(required=True)
    number_of_passenger_seats = ndb.IntegerProperty(required=True)
    active = ndb.BooleanProperty(required=True)


def GetAllUserTransferRequests(application_user_id):
    return Transfer.query(Transfer.application_user_id == application_user_id).order(
        -Transfer.created).fetch(20)


def GetTransferByID(transfer_id):
    return Transfer.get_by_id(transfer_id)


def GetAllActiveTransfers():
    return Transfer.query(Transfer.active == True).fetch(limit=None)


def StartNewTransfer(transfer_vehicle_id, transfer_route_id, pricing_option, amount):
    transfer_vehicle_entity = TransferVehicle.GetTransferVehicleByID(long(transfer_vehicle_id))
    transfer_route_entity = TransferRoute.GetTransferRouteByID(long(transfer_route_id))
    application_user = ApplicationUser.GetApplicationByUserID(transfer_route_entity.application_user_id)
    transfer = Transfer(transfer_vehicle=transfer_vehicle_id,
                        transfer_route=transfer_route_id,
                        pricing_option=pricing_option,
                        amount=amount,
                        application_user_id=transfer_route_entity.application_user_id,
                        number_of_passenger_seats=transfer_vehicle_entity.number_of_passenger_seats,
                        active=True)

    transfer_key = transfer.put()

    for index, transfer_stop in enumerate(transfer_route_entity.transfer_stops):
        transfer_stop_entity = TransferStop(transfer_id=transfer_key.id(),
                                            meeting_point_id=transfer_stop,
                                            order_id=index + 1)
        transfer_stop_entity.put()

    Utils.SendStartLocationMonitoringToCloudMessaging(application_user)

    return transfer