__author__ = 'Nejc'

from google.appengine.ext import ndb


class ApplicationUser(ndb.Model):
    unique_user_id = ndb.StringProperty(required=True)
    email = ndb.StringProperty(required=True)
    name = ndb.StringProperty(required=True)
    created = ndb.DateTimeProperty(auto_now_add=True)
    last_sign_in = ndb.DateTimeProperty(auto_now=True)
    device_registration_id = ndb.StringProperty()
    bitcoin_address = ndb.StringProperty()


def GetByEmail(email):
    return ApplicationUser.query(ApplicationUser.email == email).get()


def GetOrRegisterApplicationUser(user):
    if user:
        application_user = ApplicationUser.get_by_id(user.user_id())
        if not application_user:
            application_user = ApplicationUser(id=user.user_id(),
                                               unique_user_id=user.user_id(),
                                               email=user.email(),
                                               name=user.nickname())
        application_user.put()
        return application_user
    return None


def GetApplicationUserNickname(application_user):
    if application_user:
        return application_user.name
    return ''


def GetApplicationByUserID(application_user_id):
    return ApplicationUser.query(ApplicationUser.unique_user_id == application_user_id).get()
