from google.appengine.ext import ndb

__author__ = 'Nejc'


class UserLocationData(ndb.Model):
    application_user_id = ndb.StringProperty(required=True)
    email = ndb.StringProperty(required=True)
    latitude = ndb.FloatProperty(required=True)
    longitude = ndb.FloatProperty(required=True)
    saved = ndb.DateTimeProperty(auto_now_add=True)
    geo_location = ndb.GeoPtProperty(required=True)


def GetLastUserLocations(application_user_id, number_of_locations):
    return UserLocationData.query(UserLocationData.application_user_id == application_user_id).order(
        -UserLocationData.saved).fetch(number_of_locations)
