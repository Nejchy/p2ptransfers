__author__ = 'Nejc'

import logging
import traceback
from datetime import datetime
from google.appengine.api import search

_INDEX_NAME = 'MeetingPoint'
_FIELD_NAME = 'name'
_FIELD_LOCATION = 'geo_location'
_FIELD_LATITUDE = 'latitude'
_FIELD_LONGITUDE = 'longitude'
_FIELD_DATE = 'date'


def PutDocument(name, latitude, longitude):
    try:
        document = CreateDocument(name, latitude, longitude)
        search.Index(name=_INDEX_NAME).put(document)
        logging.debug('Document inserted')
    except search.Error:
        logging.debug('Error inserting document' +
                      traceback.print_stack())


def GetLatitude(document):
    for field in document.fields:
        if field.name == _FIELD_LATITUDE:
            return field.value
    return None


def GetLongitude(document):
    for field in document.fields:
        if field.name == _FIELD_LONGITUDE:
            return field.value
    return None


def GetMeetingPointName(document):
    for field in document.fields:
        if field.name == _FIELD_NAME:
            return field.value
    return ''

def CreateDocument(name, latitude, longitude):
    geo_point = search.GeoPoint(latitude, longitude)
    return search.Document(
        fields=[search.TextField(name=_FIELD_NAME, value=name),
                search.GeoField(name=_FIELD_LOCATION, value=geo_point),
                search.DateField(name=_FIELD_DATE, value=datetime.now()),
                search.NumberField(name=_FIELD_LATITUDE, value=latitude),
                search.NumberField(name=_FIELD_LONGITUDE, value=longitude)])


def GetMeetingPointsInsideRadius(latitude, longitude, limit, radius_in_meters):
    loc_expr = 'distance(geo_location, geopoint(' + str(latitude) + ', ' + str(longitude) + '))'
    query = 'distance(geo_location, geopoint(' + str(latitude) + ', ' + str(longitude) + ')) < ' + str(radius_in_meters)

    expr_list = [search.SortExpression(
        expression=loc_expr,
        direction=search.SortExpression.ASCENDING,
        default_value=radius_in_meters + 1)]

    sort_opts = search.SortOptions(
        expressions=expr_list)

    query_options = search.QueryOptions(
        limit=limit,
        sort_options=sort_opts)

    query_obj = search.Query(query_string=query, options=query_options)
    try:
        return search.Index(name=_INDEX_NAME).search(query=query_obj)
    except:
        return [CreateDocument('test1', 45.775864, 14.21366)]


def GetAllMeetingPoints():
    query = _FIELD_DATE + ' > 2000-1-1'
    expr_list = [search.SortExpression(
        expression=_FIELD_NAME,
        direction=search.SortExpression.ASCENDING,
        default_value='_')]

    sort_opts = search.SortOptions(
        expressions=expr_list)

    query_options = search.QueryOptions(
        sort_options=sort_opts)

    query_obj = search.Query(options=query_options, query_string=query)
    try:
        return search.Index(name=_INDEX_NAME).search(query_obj)
    except:
        return [CreateDocument('test1', 45.775864, 14.21366)]


def GetDocumentByDocID(document_id):
    try:
        document = search.Index(name=_INDEX_NAME).get(document_id)
        logging.info(document is None)
        if document is None:
            return GetTestDocument()
        return document
    except:
        return GetTestDocument()


def GetDocumentByName(name):
    try:
        documents = search.Index(name=_INDEX_NAME).search(name)
        if documents is None or documents.number_found == 0:
            return GetTestDocument()
        elif documents.number_found > 0:
            return documents.results[0]
    except:
        GetTestDocument()


def GetDocumentInsideBounds(upper_left_point, down_right_point):
    try:
        query = _FIELD_LATITUDE + ' <= ' + str(upper_left_point.latitude) + ' AND ' + \
                _FIELD_LATITUDE + ' >= ' + str(down_right_point.latitude) + ' AND ' + \
                _FIELD_LONGITUDE + ' <= ' + str(upper_left_point.longitude) + ' AND ' + \
                _FIELD_LONGITUDE + ' >= ' + str(down_right_point.longitude)
        logging.info(query)
        query_obj = search.Query(query_string=query)
        return search.Index(name=_INDEX_NAME).search(query=query_obj)
    except:
        logging.exception("search exception")
        return [CreateDocument('test1', 45.775864, 14.213661)]


def DeleteDocumentByID(document_id):
    doc_index = search.Index(name=_INDEX_NAME)
    doc_index.delete(document_id)


def GetTestDocument():
    return CreateDocument('test1', 45.0500, 14.5000)