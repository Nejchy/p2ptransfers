__author__ = 'Nejc'

from google.appengine.ext import db


class TransferRoute(db.Model):
    name = db.StringProperty(required=True)
    application_user_id = db.StringProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    travelling_to = db.StringProperty(required=True)
    transfer_stops = db.StringListProperty()


def GetTransferRouteForUser(application_user_id):
    q = db.GqlQuery("SELECT * FROM TransferRoute " +
                    "WHERE application_user_id = :1 " +
                    "ORDER BY name DESC",
                    application_user_id)
    return q.fetch(100)


def GetTransferRouteByID(transfer_route_id):
    return TransferRoute.get_by_id(transfer_route_id)