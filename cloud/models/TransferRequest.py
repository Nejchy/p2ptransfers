__author__ = 'Nejc'

from google.appengine.ext import ndb


class TransferRequest(ndb.Model):
    transfer_id = ndb.StringProperty(required=True)
    meeting_point_id = ndb.StringProperty(required=True)
    application_user_id = ndb.StringProperty(required=True)
    request_time = ndb.DateTimeProperty(auto_now_add=True)
    approved = ndb.BooleanProperty(required=True)
    driver_responded = ndb.BooleanProperty(required=True)
    notification_sent = ndb.BooleanProperty(required=True)


def GetAllUserTransferRequests(application_user_id):
    return TransferRequest.query(TransferRequest.application_user_id == application_user_id).order(
        -TransferRequest.request_time).fetch(20)


def GetTransferRequestByID(transfer_request_id):
    return TransferRequest.get_by_id(transfer_request_id)


def GetAllTransferTransferRequests(transfer__id):
    return TransferRequest.query(TransferRequest.transfer_id == transfer__id).order(
        -TransferRequest.request_time).fetch(50)