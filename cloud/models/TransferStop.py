from google.appengine.ext import ndb


class TransferStop(ndb.Model):
    transfer_id = ndb.IntegerProperty(required=True)
    meeting_point_id = ndb.StringProperty(required=True)
    order_id = ndb.IntegerProperty(required=True)


def GetTransfersForMeetingPointID(meeting_point_id):
    return TransferStop.query(TransferStop.meeting_point_id == meeting_point_id).fetch(limit=None)


def GetNextTransferStop(transfer_id, number_of_transfer_stops):
    return TransferStop.query(TransferStop.transfer_id == long(transfer_id)).order(TransferStop.order_id).fetch(
        number_of_transfer_stops)
