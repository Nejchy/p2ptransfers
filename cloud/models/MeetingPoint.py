__author__ = 'Nejc'

from google.appengine.ext import db


class MeetingPoint(db.Model):
    name = db.StringProperty(required=True)
    geo_location = db.GeoPtProperty(required=True)