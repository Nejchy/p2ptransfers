from google.appengine.ext import ndb
from business_logic import TransferSchedulingOption


class ScheduleTransfer(ndb.Model):
    scheduling_option = ndb.IntegerProperty(required=True)
    schedule_time = ndb.TimeProperty(required=True)
    last_scheduled_date = ndb.DateProperty(auto_now=True)
    last_scheduled_time = ndb.TimeProperty(auto_now=True)
    transfer_vehicle_id = ndb.StringProperty(required=True)
    transfer_route_id = ndb.StringProperty(required=True)
    pricing_option_id = ndb.IntegerProperty(required=True)
    amount = ndb.FloatProperty(required=True)
    created = ndb.DateTimeProperty(auto_now_add=True)
    application_user_id = ndb.StringProperty(required=True)

    def ShouldTransferBeStarted(self, datetime):
        should_run_today = TransferSchedulingOption.ShouldRunToday(self.scheduling_option, datetime.isoweekday())

        if should_run_today and self.schedule_time < datetime.time():
            return True
        return False


def GetTransferThatWereNotStartedOnDate(datetime):
    return ScheduleTransfer.query(ScheduleTransfer.last_scheduled_date < datetime.date()).fetch(limit=None)


def GetAllScheduledUserTransfers(application_user_id):
    return ScheduleTransfer.query(ScheduleTransfer.application_user_id == application_user_id).fetch(limit=None)