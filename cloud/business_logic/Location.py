import webapp2


class Location:
    def __init__(self, lat, lon, title, doc_id):
        self.lat = lat
        self.lon = lon
        self.title = title
        self.doc_id = doc_id
        self.link = webapp2.uri_for('MeetingPoint', document_id=doc_id)

    def toJSON(self):
        return '{"lat":"' + str(self.lat) + \
               '","lon":"' + str(self.lon) + \
               '","title":"' + self.title.encode('utf-8') + \
               '","doc_id":"' + str(self.doc_id) + '"}'