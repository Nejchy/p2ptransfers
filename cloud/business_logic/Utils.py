import json
import logging
from math import radians, cos, sin, asin, sqrt
from google.appengine.api import urlfetch, users
import webapp2
from models import ApplicationUser

def GetApiKey():
    return 'AIzaSyBNs-mOnuCYY_mRZjpE3s9Cle-2aReRgQE'


def GetGcmUrl():
    return 'https://android.googleapis.com/gcm/send'


def RepresentsFloat(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    km = 6367 * c
    return km


def SendTransferRequestToCloudMessaging(application_user, url_safe_key, rating, meeting_point_name):
    json_dict = {'registration_ids': [application_user.device_registration_id],
                 'data': {'message': 'Requested a transfer from ' + meeting_point_name,
                          'request_key': url_safe_key,
                          'rating': rating,
                          'meeting_point_name': meeting_point_name,
                          'type': 'PUSH_NOTIFICATION_DRIVER',
                          'email': application_user.email}}
    #logging.info(json_dict)
    result = urlfetch.fetch(url=GetGcmUrl(),
                            payload=json.dumps(json_dict),
                            method=urlfetch.POST,
                            headers={'Content-Type': 'application/json', 'Authorization': 'key=' + GetApiKey()})

    #logging.info(str(result.status_code))
    if result.status_code == 200:
        return True

    return False


def SendTransferResponseToCloudMessaging(application_user, vehicle_description, meeting_point_name, is_approved):
    is_approved_text = 'Denied'
    if is_approved:
        is_approved_text = 'Approved'
    json_dict = {'registration_ids': [application_user.device_registration_id],
                 'data': {'message': 'Transfer from ' + meeting_point_name + ' is ' + is_approved_text,
                          'is_approved_text': is_approved_text,
                          'is_approved': is_approved,
                          'vehicle_description': vehicle_description,
                          'meeting_point_name': meeting_point_name,
                          'type': 'PUSH_NOTIFICATION_PASSENGER',
                          'email': application_user.email}}
    #logging.info(json_dict)
    result = urlfetch.fetch(url=GetGcmUrl(),
                            payload=json.dumps(json_dict),
                            method=urlfetch.POST,
                            headers={'Content-Type': 'application/json', 'Authorization': 'key=' + GetApiKey()})

    #logging.info(str(result.status_code))
    if result.status_code == 200:
        return True

    return False


def SendStartLocationMonitoringToCloudMessaging(application_user):
    json_dict = {'registration_ids': [application_user.device_registration_id],
                 'data': {'type': 'START_MONITORING', 'email': application_user.email}}
    logging.info(json_dict)
    result = urlfetch.fetch(url=GetGcmUrl(),
                            payload=json.dumps(json_dict),
                            method=urlfetch.POST,
                            headers={'Content-Type': 'application/json', 'Authorization': 'key=' + GetApiKey()})

    #logging.info(str(result.status_code))
    if result.status_code == 200:
        return True

    return False


def StopStartLocationMonitoringToCloudMessaging(application_user):
    json_dict = {'registration_ids': [application_user.device_registration_id],
                 'data': {'type': 'STOP_MONITORING'}}
    #logging.info(json_dict)
    result = urlfetch.fetch(url=GetGcmUrl(),
                            payload=json.dumps(json_dict),
                            method=urlfetch.POST,
                            headers={'Content-Type': 'application/json', 'Authorization': 'key=' + GetApiKey()})

    #logging.info(str(result.status_code))
    if result.status_code == 200:
        return True

    return False


def GetNavigationTemplateValuesForUser(handler, application_user):
    navigation_values = {
        'home_link': webapp2.uri_for('Home'),
        'meeting_points_link': webapp2.uri_for('MeetingPoints'),
        'log_in_link': users.create_login_url(handler.request.uri),
        'log_out_link': users.create_logout_url(webapp2.uri_for('Home')),
        'add_transfer_link': webapp2.uri_for('DriverOverview'),
        'transfer_requests_link': webapp2.uri_for('AllTransferRequests'),
        'transfers_link': webapp2.uri_for('Transfers'),
        'user_link': webapp2.uri_for('UserOverview'),
        'nickname': ApplicationUser.GetApplicationUserNickname(application_user),
        'signed_in': application_user is not None
    }

    return navigation_values


def GetNavigationTemplateValues(handler):
    user = users.get_current_user()
    application_user = ApplicationUser.GetOrRegisterApplicationUser(user)

    return GetNavigationTemplateValuesForUser(handler, application_user)


def RedirectIfNotSignedIn(handler):
    user = users.get_current_user()
    if user is None:
        handler.redirect(users.create_login_url(handler.request.uri))


def GetSignedInApplicationUser():
    user = users.get_current_user()
    return ApplicationUser.GetOrRegisterApplicationUser(user)
