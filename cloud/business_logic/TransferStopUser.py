class TransferStopUser:
    def __init__(self, travelling_to, pricing_option_text, price, number_of_free_seats, application_user_id, transfer_apply_link,
                 duration_value, distance_value, duration_sort, rating):
        self.travelling_to = travelling_to
        self.price = price
        self.number_of_free_seats = number_of_free_seats
        self.application_user_id = application_user_id
        self.transfer_apply_link = transfer_apply_link
        self.duration_value = duration_value
        self.distance_value = distance_value
        self.duration_sort = duration_sort
        self.rating = rating
        self.pricing_option_text = pricing_option_text

    def toJSON(self):
        return '{"travelling_to":"' + self.travelling_to.encode('utf-8') + \
               '","price":"' + str(self.price) + \
               '","number_of_free_seats":"' + str(self.number_of_free_seats) + \
               '","application_user_id":"' + str(self.application_user_id) + \
               '"}'