class TransferSchedulingOption:
    def __init__(self, option_id, name):
        self.option_id = option_id
        self.name = name


def GetSchedulingOptions():
    options = [TransferSchedulingOption(option_id=1, name=GetText(1)),
               TransferSchedulingOption(option_id=2, name=GetText(2)),
               TransferSchedulingOption(option_id=3, name=GetText(3)),
               TransferSchedulingOption(option_id=4, name=GetText(4))]
    return options


def ShouldRunToday(option_id, num_day_of_week):
    if option_id == 2:
        return True
    elif option_id == 3 and (num_day_of_week == 1 or num_day_of_week == 2 or num_day_of_week == 3
                             or num_day_of_week == 4 or num_day_of_week == 5):
        return True
    elif option_id == 4 and (num_day_of_week == 6 or num_day_of_week == 7):
        return True
    else:
        return False


def GetText(option_id):
    if option_id == 2:
        return 'Every day'
    elif option_id == 3:
        return 'Every workday'
    elif option_id == 4:
        return 'On weekend'
    else:
        return 'No schedule'