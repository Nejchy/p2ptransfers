__author__ = 'Nejc'


class PricingOption:
    def __init__(self, option_id, name):
        self.option_id = option_id
        self.name = name


def GetPricingOptions():
    price_options = [PricingOption(option_id=1, name=GetText(1)),
                     PricingOption(option_id=2, name=GetText(2)),
                     PricingOption(option_id=3, name=GetText(3))]
    return price_options


def CalculateAmount(transfer, distance_in_km):
    if transfer.pricing_option == 2:
        return transfer.amount
    elif transfer.pricing_option == 3:
        return transfer.amount * distance_in_km
    else:
        return 0


def GetText(option_id):
    if option_id == 2:
        return 'Fixed'
    elif option_id == 3:
        return 'By km'
    else:
        return 'Free'