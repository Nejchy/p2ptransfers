import json
import logging
import os
import urllib
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
import jinja2
import webapp2
from business_logic import Utils, PricingOption
from models import Payment, MeetingPointDocument, Transfer, ApplicationUser


jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(
                                           os.path.join(os.path.dirname(__file__), '..', 'templates')))


class CreatePayment(webapp2.RequestHandler):
    def get(self, transfer_request_key):
        entity_key = ndb.Key(urlsafe=transfer_request_key)
        transfer_request = entity_key.get()
        meeting_point = MeetingPointDocument.GetDocumentByDocID(transfer_request.meeting_point_id)

        transfer_request.start_location = MeetingPointDocument.GetMeetingPointName(meeting_point)
        transfer_request.safe_key = transfer_request.key.urlsafe()

        payment = Payment.GetPaymentByID(entity_key.id())
        if payment is None:
            payment = Payment.Payment(id=entity_key.id(),
                                      payment_confirmed=False,
                                      payment_made=False,
                                      distance_travelled=0,
                                      amount=0)
            payment.put()
            payment.is_confirmed = False
        else:
            payment.is_confirmed = True

        template_navigation_values = Utils.GetNavigationTemplateValues(self)

        template_specific_values = {
            'transfer_request': transfer_request,
            'payment': payment,
            'title': 'Initialize payment',
            'calculate_distance_link': webapp2.uri_for('CalculatePaymentDistance'),
            'calculate_amount_link': webapp2.uri_for('CalculatePaymentAmount'),
            'payment_post_link': webapp2.uri_for('CreatePayment', transfer_request_key=transfer_request_key)
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('Payment.html')
        self.response.out.write(template.render(template_values))

    def post(self, transfer_request_key):
        amount = float(self.request.get('transfer_payment_amount'))
        transfer_distance = float(self.request.get('transfer_distance'))

        entity_key = ndb.Key(urlsafe=transfer_request_key)
        transfer_request = entity_key.get()
        transfer = Transfer.GetTransferByID(long(transfer_request.transfer_id))

        if amount == 0:
            transfer.number_of_passenger_seats += 1
            transfer.put()

            payment = Payment.GetPaymentByID(long(entity_key.id()))

            payment.amount = amount
            payment.distance_travelled = transfer_distance
            payment.payment_made = True
            payment.put()
        else:
            driver_user = ApplicationUser.GetApplicationByUserID(transfer.application_user_id)

            callback_url = urllib.quote_plus(
                self.request.host_url + webapp2.uri_for('PaymentCallback', payment=entity_key.id()))
            address = driver_user.bitcoin_address

            url = "https://blockchain.info/api/receive?method=create&address={0}&callback={1}"
            url = url.format(address, callback_url)

            result = urlfetch.fetch(url, method=urlfetch.GET)

            if result.status_code == 200:
                transfer.number_of_passenger_seats += 1
                transfer.put()

                payment_data = json.loads(result.content)
                payment = Payment.GetPaymentByID(long(entity_key.id()))

                payment.receive_address = payment_data['input_address']
                payment.amount = amount
                payment.distance_travelled = transfer_distance
                payment.put()
            else:
                logging.debug(' failed bitcoin api request ' + str(result.status_code))

        self.redirect(webapp2.uri_for('CreatePayment', transfer_request_key=transfer_request_key))


class ConfirmPayment(webapp2.RequestHandler):
    def get(self, transfer_request_key):
        logging.debug('confirm payment ' + transfer_request_key)
        entity_key = ndb.Key(urlsafe=transfer_request_key)
        #transfer_request = entity_key.get()
        payment = Payment.GetPaymentByID(entity_key.id())
        payment.payment_confirmed = True
        payment.put()
        self.redirect(
            webapp2.uri_for('TransferRequestOverview', transfer_request_id=entity_key.id(), PaymentConfirmed='1'))


class CalculateDistance(webapp2.RequestHandler):
    def post(self):
        logging.info(self.request.body)
        payment_data = json.loads(self.request.body)
        request_key = payment_data['request_key']
        latitude = payment_data['lat']
        longitude = payment_data['lon']

        entity_key = ndb.Key(urlsafe=request_key)
        transfer_request = entity_key.get()
        meeting_point = MeetingPointDocument.GetDocumentByDocID(transfer_request.meeting_point_id)
        transfer_request.meeting_point_name = MeetingPointDocument.GetMeetingPointName(meeting_point)
        transfer = Transfer.GetTransferByID(long(transfer_request.transfer_id))
        url = "http://maps.googleapis.com/maps/api/directions/json?" \
              "origin={0},{1}&destination={2},{3}&units=metric&sensor=false"
        url = url.format(latitude,
                         longitude,
                         MeetingPointDocument.GetLatitude(meeting_point),
                         MeetingPointDocument.GetLongitude(meeting_point))

        logging.debug(url)

        result = urlfetch.fetch(url)
        logging.info('status code ' + str(result.status_code))
        if result.status_code == 200:
            location_data = json.loads(result.content)
            leg = location_data['routes'][0]['legs'][0]
            #distance_value = leg['distance']['text']
            distance_sort = float(leg['distance']['value'])
            distance_sort /= 1000
            amount = PricingOption.CalculateAmount(transfer, distance_sort)
            return_json = '{ "distance": ' + str(distance_sort) + ', "amount": ' + str(amount) + '}'
            logging.debug(return_json)
            self.response.out.write(return_json)


class CalculateAmount(webapp2.RequestHandler):
    def post(self):
        logging.info(self.request.body)
        payment_data = json.loads(self.request.body)
        request_key = payment_data['request_key']
        distance = float(payment_data['distance'])
        entity_key = ndb.Key(urlsafe=request_key)
        transfer_request = entity_key.get()
        transfer = Transfer.GetTransferByID(long(transfer_request.transfer_id))
        amount = PricingOption.CalculateAmount(transfer, distance)
        return_json = '{ "amount": ' + str(amount) + '}'
        logging.debug(return_json)
        self.response.out.write(return_json)


class Callback(webapp2.RequestHandler):
    def get(self, payment):
        logging.debug(payment)
        payment_entity = Payment.GetPaymentByID(long(payment))
        payment_entity.payment_made = True
        payment_entity.put()
        #https://blockchain.info/api/api_receive
        logging.debug('payment amount ' + str(payment_entity.amount))
        logging.debug('recieved payment ' + str(self.request.get('value')))
        self.response.out.write('*ok*')

