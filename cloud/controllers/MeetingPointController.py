import logging
import os
import urllib
import json

import webapp2
import jinja2
from google.appengine.api import urlfetch
from business_logic.Location import Location
from models import MeetingPointDocument, ApplicationUserRating, UserLocationData
from business_logic import Utils, PricingOption
from business_logic.TransferStopUser import TransferStopUser
from models.Transfer import Transfer
from models.TransferRoute import TransferRoute
from models.TransferStop import GetTransfersForMeetingPointID


jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(
                                           os.path.join(os.path.dirname(__file__), '..', 'templates')))


class MeetingPoints(webapp2.RequestHandler):
    def get(self):
        locations = []
        nearby_meeting_points = MeetingPointDocument.GetAllMeetingPoints()
        for meeting_point in nearby_meeting_points:
            temp_location = Location(MeetingPointDocument.GetLatitude(meeting_point),
                                     MeetingPointDocument.GetLongitude(meeting_point),
                                     MeetingPointDocument.GetMeetingPointName(meeting_point),
                                     meeting_point.doc_id)
            locations.append(temp_location)

        template_navigation_values = Utils.GetNavigationTemplateValues(self)
        template_specific_values = {
            'add_meeting_point_link': webapp2.uri_for('AddMeetingPoint'),
            'meeting_point_list': locations,
            'meeting_point_added': self.request.get('MeetingPointAdded') == '1',
            'title': 'Meeting points'
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('MeetingPoints.html')
        self.response.out.write(template.render(template_values))


class AddMeetingPoint(webapp2.RequestHandler):
    def get(self):
        Utils.RedirectIfNotSignedIn(self)

        template_navigation_values = Utils.GetNavigationTemplateValues(self)
        template_specific_values = {
            'add_meeting_point_link': webapp2.uri_for('AddMeetingPoint'),
            'title': 'Add meeting point'
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('AddMeetingPoint.html')
        self.response.out.write(template.render(template_values))

    def post(self):
        name = self.request.get('location_name')
        longitude = self.request.get('longitude')
        latitude = self.request.get('latitude')
        #meeting_point = MeetingPoint(name=name, geo_location=db.GeoPt(latitude, longitude))
        #meeting_point.put()
        MeetingPointDocument.PutDocument(name, float(latitude), float(longitude))
        self.redirect(webapp2.uri_for('MeetingPoints', MeetingPointAdded='1'))


class DeleteMeetingPointHandler(webapp2.RequestHandler):
    def get(self):
        template_values = {
            'delete_meeting_point_link': webapp2.uri_for('DeleteMeetingPoint')
        }

        template = jinja_environment.get_template('DeleteMeetingPoint.html')
        self.response.out.write(template.render(template_values))

    def post(self):
        MeetingPointDocument.DeleteDocumentByID(self.request.get('document_id'))
        self.redirect(webapp2.uri_for('Home'))


class MeetingPointHandler(webapp2.RequestHandler):
    def get(self, document_id):
        document = MeetingPointDocument.GetDocumentByDocID(urllib.unquote(document_id))
        transfers = GetTransfersForMeetingPointID(document_id)
        user = Utils.GetSignedInApplicationUser()
        transfer_stop_users = []

        for transfer in transfers:
            transfer_entity = Transfer.get_by_id(long(transfer.transfer_id))
            #do not show transfers started with signed in user
            if not transfer_entity.active or transfer_entity.number_of_passenger_seats == 0 or \
                    (user is not None and transfer_entity.application_user_id == user.unique_user_id):
                continue

            transfer_route = TransferRoute.get_by_id(long(transfer_entity.transfer_route))
            driver_rating = ApplicationUserRating.CalculateDriverRating(transfer_entity.application_user_id)

            user_locations = UserLocationData.GetLastUserLocations(transfer_route.application_user_id, 1)

            url = "http://maps.googleapis.com/maps/api/directions/json?origin={0},{1}&destination={2},{3}&units=metric&sensor=false"
            duration_value = 'N/A'
            distance_value = 'N/A'
            duration_sort = 10000000
            if user_locations:
                url = url.format(user_locations[0].latitude,
                                 user_locations[0].longitude,
                                 MeetingPointDocument.GetLatitude(document),
                                 MeetingPointDocument.GetLongitude(document))
                result = urlfetch.fetch(url)
                logging.info('status code ' + str(result.status_code))
                if result.status_code == 200:
                    location_data = json.loads(result.content)
                    leg = location_data['routes'][0]['legs'][0]
                    duration_value = leg['duration']['text']
                    duration_sort = leg['duration']['value']
                    distance_value = leg['distance']['text']

            logging.info('duration_value' + str(duration_value))
            logging.info('distance_value' + str(distance_value))

            transfer_stop_users.append(TransferStopUser(transfer_route.travelling_to,
                                                        PricingOption.GetText(transfer_entity.pricing_option),
                                                        transfer_entity.amount,
                                                        transfer_entity.number_of_passenger_seats,
                                                        transfer_route.application_user_id,
                                                        webapp2.uri_for('AddTransferRequest',
                                                                        transfer_id=transfer.transfer_id,
                                                                        meeting_point_id=transfer.meeting_point_id),
                                                        duration_value,
                                                        distance_value,
                                                        duration_sort,
                                                        driver_rating))
            transfer_stop_users.sort(key=lambda x: x.duration_sort)

        template_navigation_values = Utils.GetNavigationTemplateValues(self)
        template_specific_values = {
            'meeting_point_name': document.fields[0].value,
            'title': 'Meeting point',
            'transfer_stop_users': transfer_stop_users
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('MeetingPoint.html')
        self.response.out.write(template.render(template_values))
