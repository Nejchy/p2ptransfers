import datetime
import webapp2
from business_logic import Utils
from models import ScheduleTransfer, Transfer, UserLocationData, MeetingPointDocument, TransferStop, ApplicationUser


class StartScheduledTransfers(webapp2.RequestHandler):
    def get(self):
        currentDateTime = datetime.datetime.now()
        not_started_transfers = ScheduleTransfer.GetTransferThatWereNotStartedOnDate(currentDateTime)

        for scheduled_transfer in not_started_transfers:
            if scheduled_transfer.ShouldTransferBeStarted(currentDateTime):
                scheduled_transfer.put()

                Transfer.StartNewTransfer(scheduled_transfer.transfer_vehicle_id,
                                          scheduled_transfer.transfer_route_id,
                                          scheduled_transfer.pricing_option_id,
                                          scheduled_transfer.amount)


class RefreshTransferStops(webapp2.RequestHandler):
    def get(self):
        for transfer in Transfer.GetAllActiveTransfers():
            transfer_stop = TransferStop.GetNextTransferStop(transfer.key.id(), 1)

            if not transfer_stop:
                transfer.active = False
                transfer.put()
                Utils.StopStartLocationMonitoringToCloudMessaging(
                    ApplicationUser.GetApplicationByUserID(transfer.application_user_id))
            else:
                last_user_locations = UserLocationData.GetLastUserLocations(transfer.application_user_id, 2)
                document = MeetingPointDocument.GetDocumentByDocID(transfer_stop[0].meeting_point_id)
                if len(last_user_locations) == 2:
                    firstPointToMeetingPoint = Utils.haversine(last_user_locations[0].longitude,
                                                               last_user_locations[0].latitude,
                                                               MeetingPointDocument.GetLongitude(document),
                                                               MeetingPointDocument.GetLatitude(document))
                    secondPointToMeetingPoint = Utils.haversine(last_user_locations[1].longitude,
                                                                last_user_locations[1].latitude,
                                                                MeetingPointDocument.GetLongitude(document),
                                                                MeetingPointDocument.GetLatitude(document))

                    if secondPointToMeetingPoint > firstPointToMeetingPoint + 0.5:
                        transfer_stop.delete()