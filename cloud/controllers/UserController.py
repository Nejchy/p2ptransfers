import os
from google.appengine.api import users
from google.appengine.ext import ndb
import jinja2
import webapp2
from business_logic import Utils, TransferSchedulingOption
from models import ApplicationUser, ApplicationUserRating, ScheduleTransfer, TransferRoute, TransferVehicle

jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(
                                           os.path.join(os.path.dirname(__file__), '..', 'templates')))


class UserOverview(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)
        application_user.driver_rating = ApplicationUserRating.CalculateDriverRating(application_user.unique_user_id)
        application_user.passenger_rating = ApplicationUserRating.CalculatePassengerRating(
            application_user.unique_user_id)

        if application_user.bitcoin_address is None:
            application_user.bitcoin_address = ''

        scheduled_transfers = ScheduleTransfer.GetAllScheduledUserTransfers(application_user.unique_user_id)
        for scheduled_transfer in scheduled_transfers:
            scheduled_transfer.route_name = TransferRoute.GetTransferRouteByID(
                long(scheduled_transfer.transfer_route_id)).name
            scheduled_transfer.vehicle_name = TransferVehicle.GetTransferVehicleByID(
                long(scheduled_transfer.transfer_vehicle_id)).getDescription()
            scheduled_transfer.schedule_type_name = TransferSchedulingOption.GetText(
                scheduled_transfer.scheduling_option)
            scheduled_transfer.remove_link = webapp2.uri_for('RemoveScheduledTransfer',
                                                             schedule_transfer_key=scheduled_transfer.key.urlsafe())

        template_navigation_values = Utils.GetNavigationTemplateValuesForUser(self, application_user)
        template_specific_values = {
            'application_user': application_user,
            'title': 'User overview',
            'bitcoin_address_saved': self.request.get('AddressSaved') == '1',
            'scheduled_transfers': scheduled_transfers
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('UserOverview.html')
        self.response.out.write(template.render(template_values))

    def post(self):
        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)
        application_user.bitcoin_address = self.request.get('bitcoin_address')
        application_user.put()
        self.redirect(webapp2.uri_for('UserOverview', AddressSaved=1))


class RemoveScheduledTransfer(webapp2.RequestHandler):
    def get(self, schedule_transfer_key):
        rev_key = ndb.Key(urlsafe=schedule_transfer_key)
        rev_key.delete()
        self.redirect(webapp2.uri_for('UserOverview'))
