import json
import os
import webapp2
import jinja2
from business_logic.Location import Location
from models import MeetingPointDocument, Transfer
from business_logic import Utils


jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(
                                           os.path.join(os.path.dirname(__file__), '..', 'templates')))


class MainHandler(webapp2.RequestHandler):
    def get(self):
        navigation_values = Utils.GetNavigationTemplateValues(self)

        template_specific_values = {
            'title': 'P2P Transfers'
        }

        template_values = dict(navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('Main.html')
        self.response.out.write(template.render(template_values))


class ClosestMeetingPointsHandler(webapp2.RequestHandler):
    def post(self):
        self.response.out.headers['Content-Type'] = 'application/json'
        location_data = json.loads(self.request.body)
        nearby_points = []

        latitude = location_data['lat']
        longitude = location_data['lon']
        nearby_meeting_points = MeetingPointDocument.GetMeetingPointsInsideRadius(latitude, longitude, 10, 1000000)
        for meeting_point in nearby_meeting_points:
            temp_location = Location(MeetingPointDocument.GetLatitude(meeting_point),
                                     MeetingPointDocument.GetLongitude(meeting_point),
                                     MeetingPointDocument.GetMeetingPointName(meeting_point),
                                     meeting_point.doc_id)
            nearby_points.append(temp_location)

        locations_JSON = '['
        for location in nearby_points:
            locations_JSON += location.toJSON()
            locations_JSON += ','
        locations_JSON = locations_JSON.rstrip(',')
        locations_JSON += ']'
        self.response.out.write(locations_JSON)


class TestHandler(webapp2.RequestHandler):
    def get(self):
        navigation_values = Utils.GetNavigationTemplateValues(self)
        transfer_entity = Transfer.GetTransferByID(4888690690097152)
        transfer_entity.number_of_passenger_seats = 3
        transfer_entity.put()

        transfer_entity = Transfer.GetTransferByID(5170165666807808)
        transfer_entity.number_of_passenger_seats = 1
        transfer_entity.put()

        transfer_entity = Transfer.GetTransferByID(5677555923288064)
        transfer_entity.number_of_passenger_seats = 2
        transfer_entity.put()

        transfer_entity = Transfer.GetTransferByID(6201404494446592)
        transfer_entity.number_of_passenger_seats = 1
        transfer_entity.put()
#
#         template_specific_values = {
#             'transfer_started': self.request.get('TransferStarted') == '1',
#             'title': 'test page'
#         }
#
#         template_values = dict(navigation_values)
#         template_values.update(template_specific_values)
#
#         template = jinja_environment.get_template('Test.html')
#         self.response.out.write(template.render(template_values))
