import logging
import os
import datetime
from google.appengine.api import users
import jinja2
import webapp2
from business_logic import Utils, PricingOption, TransferSchedulingOption
from models import Transfer, ApplicationUser, TransferRoute, TransferVehicle, TransferRequest, MeetingPointDocument, \
    ApplicationUserRating, Payment, ScheduleTransfer

jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(
                                           os.path.join(os.path.dirname(__file__), '..', 'templates')))


class AddTransfer(webapp2.RequestHandler):
    def get(self):
        Utils.RedirectIfNotSignedIn(self)

        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)

        transfer_vehicles = []
        transfer_routes = []
        if application_user:
            transfer_vehicles = TransferVehicle.GetTransferVehiclesForUser(application_user.unique_user_id)
            transfer_routes = TransferRoute.GetTransferRouteForUser(application_user.unique_user_id)

        price_options = PricingOption.GetPricingOptions()
        schedule_options = TransferSchedulingOption.GetSchedulingOptions()

        navigation_values = Utils.GetNavigationTemplateValuesForUser(self, application_user)

        template_specific_values = {
            'add_transfer_route_link': webapp2.uri_for('AddTransferRoute'),
            'add_transfer_vehicle_link': webapp2.uri_for('AddTransferVehicle'),
            'title': 'Start a transfer',
            'transfer_vehicle_added': self.request.get('TransferVehicleAdded') == '1',
            'transfer_route_added': self.request.get('TransferRouteAdded') == '1',
            'transfer_vehicles': transfer_vehicles,
            'transfer_routes': transfer_routes,
            'price_options': price_options,
            'schedule_options': schedule_options
        }

        template_values = dict(navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('AddTransfer.html')
        self.response.out.write(template.render(template_values))

    def post(self):
        transfer_vehicle = self.request.get('transfer_vehicle')
        transfer_route = self.request.get('transfer_route')
        pricing_option = int(self.request.get('pricing_option'))
        schedule_option = int(self.request.get('schedule_option'))
        logging.debug('schedule option ' + str(schedule_option))
        amount = 0

        if Utils.RepresentsFloat(self.request.get('payment_amount')):
            amount = float(self.request.get('payment_amount'))

        transfer = Transfer.StartNewTransfer(transfer_vehicle, transfer_route, pricing_option, amount)
        if schedule_option != 1:
            hour = int(self.request.get('schedule_hour'))
            minute = int(self.request.get('schedule_time'))
            logging.debug('hour ' + str(hour))
            logging.debug('minute ' + str(minute))
            schedule_transfer = ScheduleTransfer.ScheduleTransfer(scheduling_option=schedule_option,
                                                                  schedule_time=datetime.time(hour, minute),
                                                                  transfer_vehicle_id=transfer_vehicle,
                                                                  transfer_route_id=transfer_route,
                                                                  pricing_option_id=pricing_option,
                                                                  amount=amount,
                                                                  application_user_id=transfer.application_user_id)
            schedule_transfer.put()

        self.redirect(webapp2.uri_for('Transfers', TransferStarted='1'))


class AllUserTransfers(webapp2.RequestHandler):
    def get(self):
        Utils.RedirectIfNotSignedIn(self)
        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)

        transfers = Transfer.GetAllUserTransferRequests(application_user.unique_user_id)
        for transfer in transfers:
            transfer.route_name = TransferRoute.GetTransferRouteByID(long(transfer.transfer_route)).name
            transfer.vehicle_name = TransferVehicle.GetTransferVehicleByID(
                long(transfer.transfer_vehicle)).getDescription()
            transfer.link = webapp2.uri_for('TransferOverview', transfer_id=transfer.key.id())

        template_navigation_values = Utils.GetNavigationTemplateValuesForUser(self, application_user)
        template_specific_values = {
            'transfer_list': transfers,
            'transfer_started': self.request.get('TransferStarted') == '1',
            'title': 'Transfers'
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('Transfers.html')
        self.response.out.write(template.render(template_values))


class TransferOverview(webapp2.RequestHandler):
    def get(self, transfer_id):
        Utils.RedirectIfNotSignedIn(self)
        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)
        transfer = Transfer.GetTransferByID(long(transfer_id))
        transfer_route = TransferRoute.GetTransferRouteByID(long(transfer.transfer_route))
        transfer_vehicle = TransferVehicle.GetTransferVehicleByID(long(transfer.transfer_vehicle))

        transfer.pricing_option_text = PricingOption.GetText(transfer.pricing_option)
        transfer.payment_amount = transfer.amount
        transfer.vehicle_name = transfer_vehicle.getDescription()
        transfer.route_name = transfer_route.name

        approved_transfer_requests = []
        transfer_requests = TransferRequest.GetAllTransferTransferRequests(transfer_id)
        for transfer_request in transfer_requests:
            if transfer_request.approved:
                given_rating = ApplicationUserRating.GetRatingForVoteingUserIfExists(transfer_request.key.id(),
                                                                                     application_user.unique_user_id)
                if given_rating is None:
                    transfer_request.given_rating = 0
                else:
                    transfer_request.given_rating = given_rating.rating

                payment = Payment.GetPaymentByID(transfer_request.key.id())
                if payment is None:
                    transfer_request.transfer_pay_confirmed = False
                    transfer_request.transfer_pay_made = False
                else:
                    transfer_request.transfer_pay_confirmed = payment.payment_confirmed
                    transfer_request.transfer_pay_made = payment.payment_made

                meeting_point_document = MeetingPointDocument.GetDocumentByDocID(transfer_request.meeting_point_id)
                transfer_request.start_location = MeetingPointDocument.GetMeetingPointName(meeting_point_document)
                transfer_request.link = webapp2.uri_for('TransferRequestOverview',
                                                        transfer_request_id=transfer_request.key.id())
                approved_transfer_requests.append(transfer_request)

        template_navigation_values = Utils.GetNavigationTemplateValues(self)
        template_specific_values = {
            'transfer': transfer,
            'title': 'Transfer overview',
            'transfer_requests': approved_transfer_requests,
            'payment_confirmed': self.request.get('PaymentConfirmed') == '1'
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('TransferOverview.html')
        self.response.out.write(template.render(template_values))



