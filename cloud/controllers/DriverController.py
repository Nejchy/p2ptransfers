import logging
import os
import json

import webapp2
import jinja2
from google.appengine.api import search, users

from business_logic import Utils
from business_logic.Location import Location
from models.TransferRoute import TransferRoute
from models.TransferVehicle import TransferVehicle
from models import MeetingPointDocument


jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(
                                           os.path.join(os.path.dirname(__file__), '..', 'templates')))


class AddTransferVehicle(webapp2.RequestHandler):
    def get(self):
        Utils.RedirectIfNotSignedIn(self)

        template_navigation_values = Utils.GetNavigationTemplateValues(self)
        template_specific_values = {
            'add_transfer_vehicle_link': webapp2.uri_for('AddTransferVehicle'),
            'title': 'Add transfer vehicle'
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('AddTransferVehicle.html')
        self.response.out.write(template.render(template_values))

    def post(self):
        make = self.request.get('make')
        model = self.request.get('model')
        reg_num = self.request.get('reg_num')
        color = self.request.get('color')
        num_seats = self.request.get('num_seats')
        user = users.get_current_user()

        transfer_vehicle = TransferVehicle(
            make=make,
            model=model,
            registration_number=reg_num,
            color=color,
            number_of_passenger_seats=int(num_seats),
            application_user_id=user.user_id())

        transfer_vehicle.put()
        self.redirect(webapp2.uri_for('DriverOverview', TransferVehicleAdded='1'))


class AddTransferRoute(webapp2.RequestHandler):
    def get(self):
        Utils.RedirectIfNotSignedIn(self)

        template_navigation_values = Utils.GetNavigationTemplateValues(self)
        template_specific_values = {
            'title': 'Add transfer route',
            'add_transfer_route_link': webapp2.uri_for('AddTransferRoute'),
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('AddTransferRoute.html')
        self.response.out.write(template.render(template_values))

    def post(self):
        name = self.request.get('name')
        travelling_to = self.request.get('travelling_to')
        transfer_stop = self.request.get_all('transfer_stop')
        logging.info(transfer_stop)
        transfer_route = TransferRoute(
            name=name,
            application_user_id=users.get_current_user().user_id(),
            transfer_stops=transfer_stop,
            travelling_to=travelling_to
        )
        transfer_route.put()
        self.redirect(webapp2.uri_for('DriverOverview', TransferRouteAdded='1'))


class GetLocationNearRouteHandler(webapp2.RequestHandler):
    def post(self):
        self.response.out.headers['Content-Type'] = 'application/json'
        bounds_data = json.loads(self.request.body)
        nearby_locations = []

        for item in bounds_data:
            latitudeUpperLeft = item['latUL']
            longitudeUpperLeft = item['longUL']
            up_left_point = search.GeoPoint(float(latitudeUpperLeft), float(longitudeUpperLeft))
            latitudeDownRight = item['latDR']
            longitudeDownRight = item['longDR']
            down_right_point = search.GeoPoint(float(latitudeDownRight), float(longitudeDownRight))
            nearby_meeting_points = MeetingPointDocument.GetDocumentInsideBounds(up_left_point, down_right_point)
            for meeting_point in nearby_meeting_points:
                temp_location = Location(meeting_point.fields[3].value,
                                         meeting_point.fields[4].value,
                                         meeting_point.fields[0].value,
                                         meeting_point.doc_id)
                nearby_locations.append(temp_location)

        locations_JSON = '['
        for location in nearby_locations:
            locations_JSON += location.toJSON()
            locations_JSON += ','
        locations_JSON = locations_JSON.rstrip(',')
        locations_JSON += ']'

        self.response.out.write(locations_JSON)