import logging
import json
from google.appengine.ext import db, ndb
from google.appengine.api import urlfetch
import webapp2
from business_logic import Utils
from models import ApplicationUser, Transfer, TransferVehicle, MeetingPointDocument
from models.TransferRequest import TransferRequest
from models.UserLocationData import UserLocationData


class SaveLocationData(webapp2.RequestHandler):
    def post(self):
        location_data = json.loads(self.request.body)
        email = location_data['email'].lower()
        longitude = float(location_data['longitude'])
        latitude = float(location_data['latitude'])
        #locationTime = location_data['location_time']
        #TODO add accuracy and location time

        user = ApplicationUser.GetByEmail(email)
        if user is not None:
            location = UserLocationData(application_user_id=user.unique_user_id, email=email, longitude=longitude,
                                        latitude=latitude,
                                        geo_location=db.GeoPt(latitude, longitude))
            location.put()
            self.response.set_status(201)
        else:
            logging.info(email + " not registered")
            self.response.set_status(404, "User does not exist")


class DoesUserExists(webapp2.RequestHandler):
    def post(self):
        location_data = json.loads(self.request.body)
        email = location_data['email'].lower()

        user = ApplicationUser.GetByEmail(email)
        if user is not None:
            self.response.set_status(200)
        else:
            self.response.set_status(404, "User does not exist")


class SaveAndroidDeviceRegistrationID(webapp2.RequestHandler):
    def post(self):
        logging.info(self.request.body)
        registration_data = json.loads(self.request.body)
        email = registration_data['email'].lower()
        device_registration_id = registration_data['device_registration_id']
        logging.info('email ' + email + ' reg_id ' + device_registration_id)
        user = ApplicationUser.GetByEmail(email)
        if user is not None:
            user.device_registration_id = device_registration_id
            user.put()
            self.response.set_status(201)
        else:
            logging.info(email + " not registered")
            self.response.set_status(404, "User does not exist")


class RespondToUserRequest(webapp2.RequestHandler):
    def post(self):
        logging.info(self.request.body)
        registration_data = json.loads(self.request.body)
        request_key = registration_data['request_key']
        is_approved = False
        if int(registration_data['is_approved']) == 1:
            is_approved = True

        logging.info(request_key)
        logging.info(str(is_approved))
        entity_key = ndb.Key(urlsafe=request_key)

        transfer_request = TransferRequest.get_by_id(entity_key.id())

        if not transfer_request.driver_responded or transfer_request.approved != is_approved:
            transfer_entity = Transfer.GetTransferByID(long(transfer_request.transfer_id))
            transfer_vehicle = TransferVehicle.GetTransferVehicleByID(long(transfer_entity.transfer_vehicle))
            application_user = ApplicationUser.GetApplicationByUserID(transfer_request.application_user_id)
            meeting_point_name = MeetingPointDocument.GetMeetingPointName(
                MeetingPointDocument.GetDocumentByDocID(transfer_request.meeting_point_id))
            approved = False

            if not transfer_request.approved and is_approved:
                transfer_entity.number_of_passenger_seats -= 1
                transfer_entity.put()
                approved = True
            elif transfer_request.approved and not is_approved:
                transfer_entity.number_of_passenger_seats += 1
                transfer_entity.put()
                approved = False

            transfer_request.approved = is_approved
            transfer_request.driver_responded = True
            transfer_request.put()

            Utils.SendTransferResponseToCloudMessaging(application_user,
                                                       transfer_vehicle.getDescription(),
                                                       meeting_point_name,
                                                       approved)


class Test(webapp2.RequestHandler):
    def get(self):
        result = urlfetch.fetch(
            'http://maps.googleapis.com/maps/api/directions/json?origin=45.962375,14.293736&destination=45.966667,13.65&sensor=false')
        if result.status_code == 200:
            location_data = json.loads(result.content)
            leg = location_data['routes'][0]['legs'][0]
            duration = leg['duration']
            distance = leg['distance']
            self.response.write('duration ' + duration['text'] + " " + str(duration['value']))
            self.response.write('duration ' + distance['text'] + " " + str(distance['value']))
            self.response.set_status(200)