import json
import logging
import os
from google.appengine.api import users
from google.appengine.ext import ndb
import jinja2
import webapp2
from business_logic import Utils
from models import ApplicationUser, MeetingPointDocument, Transfer, ApplicationUserRating, Payment, TransferVehicle
from models import TransferRequest

jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(
                                           os.path.join(os.path.dirname(__file__), '..', 'templates')))


class AddTransferRequest(webapp2.RequestHandler):
    def get(self, transfer_id, meeting_point_id):
        Utils.RedirectIfNotSignedIn(self)
        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)

        transfer_request = TransferRequest.TransferRequest(transfer_id=transfer_id,
                                                           meeting_point_id=meeting_point_id,
                                                           application_user_id=application_user.unique_user_id,
                                                           approved=False,
                                                           driver_responded=False,
                                                           notification_sent=False)
        transfer_request_key = transfer_request.put()
        logging.debug('transfer_request_key ' + str(transfer_request_key))
        logging.debug('transfer_request_key.urlsafe() ' + transfer_request_key.urlsafe())
        logging.debug('transfer_request_key.urlsafe() ' + str(transfer_request.key))
        meeting_point = MeetingPointDocument.GetDocumentByDocID(transfer_request.meeting_point_id)
        meeting_point_name = MeetingPointDocument.GetMeetingPointName(meeting_point)
        rating = ApplicationUserRating.CalculatePassengerRating(application_user.unique_user_id)
        transfer = Transfer.GetTransferByID(long(transfer_id))
        driver_user = ApplicationUser.GetApplicationByUserID(transfer.application_user_id)
        isNotificationSent = Utils.SendTransferRequestToCloudMessaging(driver_user, transfer_request_key.urlsafe(),
                                                                       rating,
                                                                       meeting_point_name)
        transfer_request.notification_sent = isNotificationSent
        transfer_request.put()

        self.redirect(webapp2.uri_for('AllTransferRequests', TransferRequested='1'))


class AllUserTransferRequests(webapp2.RequestHandler):
    def get(self):
        Utils.RedirectIfNotSignedIn(self)

        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)
        transfer_requests = TransferRequest.GetAllUserTransferRequests(application_user.unique_user_id)

        for transfer_request in transfer_requests:
            meeting_point = MeetingPointDocument.GetDocumentByDocID(transfer_request.meeting_point_id)
            transfer_request.meeting_point_name = MeetingPointDocument.GetMeetingPointName(meeting_point)
            transfer_request.link = webapp2.uri_for('TransferRequestOverview',
                                                    transfer_request_id=transfer_request.key.id())

        template_navigation_values = Utils.GetNavigationTemplateValuesForUser(self, application_user)

        template_specific_values = {
            'transfer_requests': transfer_requests,
            'transfer_requested': self.request.get('TransferRequested') == '1',
            'title': 'Transfer requests'}

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('TransferRequests.html')
        self.response.out.write(template.render(template_values))


class TransferRequestOverview(webapp2.RequestHandler):
    def get(self, transfer_request_id):
        Utils.RedirectIfNotSignedIn(self)
        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)

        transfer_request = TransferRequest.GetTransferRequestByID(long(transfer_request_id))
        meeting_point_document = MeetingPointDocument.GetDocumentByDocID(transfer_request.meeting_point_id)
        transfer = Transfer.GetTransferByID(long(transfer_request.transfer_id))
        transfer_vehicle = TransferVehicle.GetTransferVehicleByID(long(transfer.transfer_vehicle))
        transfer_request.start_location = MeetingPointDocument.GetMeetingPointName(meeting_point_document)
        transfer_request.safe_key = transfer_request.key.urlsafe()
        transfer_request.transfer_vehicle = transfer_vehicle.getDescription()

        driver_rating = ApplicationUserRating.GetRatingForRatedUserIfExists(long(transfer_request_id),
                                                                            transfer.application_user_id)
        passenger_rating = ApplicationUserRating.GetRatingForRatedUserIfExists(long(transfer_request_id),
                                                                               transfer_request.application_user_id)

        payment = Payment.GetPaymentByID(transfer_request.key.id())

        if payment is None:
            transfer_request.payment_confirmed = False
            transfer_request.payment_made = False
        else:
            transfer_request.payment_confirmed = payment.payment_confirmed
            transfer_request.payment_made = payment.payment_made
            transfer_request.amount = payment.amount
            transfer_request.distance = payment.distance_travelled

        if driver_rating is not None:
            transfer_request.is_driver_rated = True
            transfer_request.driver_rating = driver_rating.rating
        else:
            transfer_request.is_driver_rated = False
            transfer_request.driver_rating = 0

        if passenger_rating is not None:
            transfer_request.is_passenger_rated = True
            transfer_request.passenger_rating = passenger_rating.rating
        else:
            transfer_request.is_passenger_rated = False
            transfer_request.passenger_rating = 0

        if application_user.unique_user_id == transfer.application_user_id:
            transfer_request.who_are_we_currently_rating = 'passenger'
            transfer_request.currently_rated = transfer_request.passenger_rating
            transfer_request.is_current_passenger_rating = 1
        else:
            transfer_request.who_are_we_currently_rating = 'driver'
            transfer_request.currently_rated = transfer_request.driver_rating
            transfer_request.is_current_passenger_rating = 0

        template_navigation_values = Utils.GetNavigationTemplateValues(self)
        template_specific_values = {
            'transfer_request': transfer_request,
            'title': 'Transfer requests overview',
            'save_rating_link': webapp2.uri_for('TransferRequestAddRating'),
            'payment_link': webapp2.uri_for('CreatePayment', transfer_request_key=transfer_request.safe_key),
            'confirm_payment_link': webapp2.uri_for('ConfirmPayment', transfer_request_key=transfer_request.safe_key),
            'payment_confirmed': self.request.get('PaymentConfirmed') == '1',
            'payment': payment
        }

        template_values = dict(template_navigation_values)
        template_values.update(template_specific_values)

        template = jinja_environment.get_template('TransferRequestOverview.html')
        self.response.out.write(template.render(template_values))


class SaveRating(webapp2.RequestHandler):
    def post(self):
        user = users.get_current_user()
        application_user = ApplicationUser.GetOrRegisterApplicationUser(user)

        rating_data = json.loads(self.request.body)
        rating = int(rating_data['score'])
        is_rating_for_passenger = int(rating_data['is_rating_for_passenger'])
        transfer_request_id = rating_data['transfer_request_id']

        if is_rating_for_passenger == 0:
            is_rating_for_passenger = False
        else:
            is_rating_for_passenger = True

        transfer_request_key = ndb.Key(urlsafe=transfer_request_id)
        transfer_request = transfer_request_key.get()
        transfer = Transfer.GetTransferByID(long(transfer_request.transfer_id))
        rating_user_id = transfer_request.application_user_id

        if not is_rating_for_passenger:
            rating_user_id = transfer.application_user_id

        existing_rating = ApplicationUserRating.GetRatingForRatedUserIfExists(transfer_request.key.id(), rating_user_id)

        logging.debug(' is rating for passenger ' + str(is_rating_for_passenger))
        logging.debug(' rating_user_id ' + str(rating_user_id))
        logging.debug(' voting_user_id ' + str(application_user.unique_user_id))

        if existing_rating is None:
            user_rating = ApplicationUserRating.ApplicationUserRating(
                transfer_request_id=transfer_request_key.id(),
                rating=rating,
                rating_user_id=rating_user_id,
                voting_user_id=application_user.unique_user_id,
                is_rating_for_passenger=is_rating_for_passenger
            )
            user_rating.put()
        else:
            existing_rating.rating = rating
            existing_rating.put()