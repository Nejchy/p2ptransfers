from protorpc import messages


class LocationDataMessage(messages.Message):
    longitude = messages.FloatField(1, required=True)
    latitude = messages.FloatField(2, required=True)


class RegistrationIDMessage(messages.Message):
    registration_id = messages.StringField(1, required=True)


class TransferRequestResponseRequestMessage(messages.Message):
    request_key = messages.StringField(1, required=True)
    is_approved = messages.BooleanField(2, required=True)


class PingMessage(messages.Message):
    message = messages.BooleanField(1, required=True)