import logging

import endpoints
from protorpc import remote
from protorpc.message_types import VoidMessage
from google.appengine.ext import ndb

from API.APIMessages import LocationDataMessage, RegistrationIDMessage, TransferRequestResponseRequestMessage, PingMessage
from business_logic import Utils
from models import Transfer, MeetingPointDocument, ApplicationUser, TransferVehicle
from models.TransferRequest import TransferRequest
from models.UserLocationData import UserLocationData

WEB_CLIENT_ID = '732839601908-89a10do847s016adhf7vgjm4nq9r0bp4.apps.googleusercontent.com'
ANDROID_CLIENT_ID = '732839601908-s714f8qcnld984tkdq6nuqqqgjmvt04o.apps.googleusercontent.com'
ANDROID_AUDIENCE = WEB_CLIENT_ID

package = 'P2PTransfers'


@endpoints.api(name='p2ptransfers', version='v1', description="P2PTransfers API",
               allowed_client_ids=[WEB_CLIENT_ID, ANDROID_CLIENT_ID,
                                   endpoints.API_EXPLORER_CLIENT_ID],
               audiences=[WEB_CLIENT_ID],
               scopes=[endpoints.EMAIL_SCOPE])
class P2PTransfersAPI(remote.Service):
    @staticmethod
    def get_signed_user():
        user = endpoints.get_current_user()
        if user:
            logging.debug(user.email())
            logging.debug(user.nickname())
            logging.debug(user.user_id())
            return ApplicationUser.GetByEmail(user.email())
        return None

    @endpoints.method(VoidMessage, PingMessage,
                      path='API/Ping', http_method='GET',
                      name='p2ptransfers.Ping')
    def ping(self, request):
        return PingMessage(message=True)

    @endpoints.method(LocationDataMessage, VoidMessage,
                      path='API/SaveLocationData', http_method='POST',
                      name='p2ptransfers.SaveLocationData')
    def save_location_data(self, request):
        user = self.get_signed_user()

        if user is not None:
            location = UserLocationData(application_user_id=user.unique_user_id,
                                        email=user.email,
                                        longitude=request.longitude,
                                        latitude=request.latitude,
                                        geo_location=ndb.GeoPt(request.latitude, request.longitude))

            location.put()
            #        self.response.set_status(201)
        else:
            logging.info("not authenicated")
            raise endpoints.UnauthorizedException('Invalid token.')
        return VoidMessage()

    @endpoints.method(RegistrationIDMessage, VoidMessage,
                      path='API/SaveAndroidID', http_method='POST',
                      name='p2ptransfers.SaveAndroidID')
    def save_android_device_registration_id(self, request):
        logging.info(request.registration_id + ' device_registration_id')

        user = self.get_signed_user()
        if user is not None:
            user.device_registration_id = request.registration_id
            user.put()
        else:
            logging.info("not authenicated")
            raise endpoints.UnauthorizedException('Invalid token.')
        return VoidMessage()


    @endpoints.method(TransferRequestResponseRequestMessage, VoidMessage,
                      path='API/RespondToUserRequest', http_method='POST',
                      name='p2ptransfers.RespondToUserRequest')
    def respond_to_user_request(self, request):
        logging.info(request.request_key)
        logging.info(str(request.is_approved))
        entity_key = ndb.Key(urlsafe=request.request_key)

        transfer_request = TransferRequest.get_by_id(entity_key.id())

        if not transfer_request.driver_responded or transfer_request.approved != request.is_approved:
            transfer_entity = Transfer.GetTransferByID(long(transfer_request.transfer_id))
            transfer_vehicle = TransferVehicle.GetTransferVehicleByID(long(transfer_entity.transfer_vehicle))
            application_user = ApplicationUser.GetApplicationByUserID(transfer_request.application_user_id)
            meeting_point_name = MeetingPointDocument.GetMeetingPointName(
                MeetingPointDocument.GetDocumentByDocID(transfer_request.meeting_point_id))
            approved = False

            if not transfer_request.approved and request.is_approved:
                transfer_entity.number_of_passenger_seats -= 1
                transfer_entity.put()
                approved = True
            elif transfer_request.approved and not request.is_approved:
                transfer_entity.number_of_passenger_seats += 1
                transfer_entity.put()
                approved = False

            transfer_request.approved = request.is_approved
            transfer_request.driver_responded = True
            transfer_request.put()

            Utils.SendTransferResponseToCloudMessaging(application_user,
                                                       transfer_vehicle.getDescription(),
                                                       meeting_point_name,
                                                       approved)
        return VoidMessage()