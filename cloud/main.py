#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging

import webapp2

from controllers import MainController, TransferRequestController, TransferController, PaymentController, UserController, CronControllers
from controllers import DriverController
from controllers import MeetingPointController
from controllers import APIController


logging.getLogger().setLevel(logging.DEBUG)

app = webapp2.WSGIApplication([
    webapp2.Route(r'/', MainController.MainHandler, name='Home'),
    webapp2.Route(r'/test', MainController.TestHandler, name='test'),
    webapp2.Route(r'/ClosestMeetingPoints', MainController.ClosestMeetingPointsHandler, name='ClosestMeetingPoints'),
    webapp2.Route(r'/driver', TransferController.AddTransfer, name='DriverOverview'),
    webapp2.Route(r'/driver/transfervehicle/add', DriverController.AddTransferVehicle, name='AddTransferVehicle'),
    webapp2.Route(r'/driver/transferroute/add', DriverController.AddTransferRoute, name='AddTransferRoute'),
    webapp2.Route(r'/driver/transferroute/GetLocationNearRouteHandler', DriverController.GetLocationNearRouteHandler, name='GetLocationNearRouteHandler'),
    webapp2.Route(r'/meetingpoint/<document_id:.+>', MeetingPointController.MeetingPointHandler, name='MeetingPoint'),
    webapp2.Route(r'/meetingpoints/all', MeetingPointController.MeetingPoints, name='MeetingPoints'),
    webapp2.Route(r'/meetingpoints/add', MeetingPointController.AddMeetingPoint, name='AddMeetingPoint'),
    webapp2.Route(r'/meetingpoints/delete', MeetingPointController.DeleteMeetingPointHandler, name='DeleteMeetingPoint'),
    webapp2.Route(r'/user/rating', TransferRequestController.SaveRating, name='TransferRequestAddRating'),
    webapp2.Route(r'/transfer_request/add/<transfer_id:.+>/<meeting_point_id:.+>', TransferRequestController.AddTransferRequest, name='AddTransferRequest'),
    webapp2.Route(r'/transfer_request', TransferRequestController.AllUserTransferRequests, name='AllTransferRequests'),
    webapp2.Route(r'/transfer_request/<transfer_request_id:.+>', TransferRequestController.TransferRequestOverview, name='TransferRequestOverview'),
    webapp2.Route(r'/transfer', TransferController.AllUserTransfers, name='Transfers'),
    webapp2.Route(r'/transfer/<transfer_id:.+>', TransferController.TransferOverview, name='TransferOverview'),
    webapp2.Route(r'/payment/callback/<payment:.+>', PaymentController.Callback, name='PaymentCallback'),
    webapp2.Route(r'/payment/confirm/<transfer_request_key:.+>', PaymentController.ConfirmPayment, name='ConfirmPayment'),
    webapp2.Route(r'/payment/calculate/distance', PaymentController.CalculateDistance, name='CalculatePaymentDistance'),
    webapp2.Route(r'/payment/calculate/amount', PaymentController.CalculateAmount, name='CalculatePaymentAmount'),
    webapp2.Route(r'/payment/<transfer_request_key:.+>', PaymentController.CreatePayment, name='CreatePayment'),
    webapp2.Route(r'/user/', UserController.UserOverview, name='UserOverview'),
    webapp2.Route(r'/api/location/add', APIController.SaveLocationData, name='SaveLocationData'),
    webapp2.Route(r'/api/user/exists', APIController.DoesUserExists, name='DoesUserExists'),
    webapp2.Route(r'/api/user/addregistrationid', APIController.SaveAndroidDeviceRegistrationID, name='ApiAddUserRegistrationID'),
    webapp2.Route(r'/api/transfer_request/respond', APIController.RespondToUserRequest, name='ApiRespondToTransferRequest'),
    webapp2.Route(r'/api/test', APIController.Test, name='Test'),
    webapp2.Route(r'/cron/start_transfers', CronControllers.StartScheduledTransfers, name='StartScheduledTransfers'),
    webapp2.Route(r'/cron/maintain_transfer_stops', CronControllers.RefreshTransferStops, name='MaintainTransferStops'),
    webapp2.Route(r'/scheduled_transfers/remove/<schedule_transfer_key:.+>', UserController.RemoveScheduledTransfer, name='RemoveScheduledTransfer'),
], debug=True)
