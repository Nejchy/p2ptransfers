Getting started.

Solution is divided into 2 parts:
 - cloud: 	Contains source code for a web aplication written for Google App Engine platform.
 - android:	Contains source code for Android app.
 
Cloud
 1. Create new Google App Engine application with Authentication type Google Account API.
 2. Change application in file app.yaml from p2pprevozi to the name of your Google App Engine application.
 3. Go to Google Developers Console and enable usage of Google Cloud Messaging for Android API and Google Maps JavaScript API v3.
 4. Create new public API access key in Google Developers Console and replace key for all calls to Google Maps API in source code.
 5. To enable authentication in REST api create 2 OAuth client id's. One for web application and one for Android application.
 6. Replace WEB_CLIENT_ID and ANDROID_CLIENT_ID in P2PTransfersAPI with Client IDs generated in step 5.
 7. Generate client libraries for REST api with Google Cloud Endpoints tools.
 
Android
 1. Replace generated client libraries
 2. Replace PROJECT_NUMBER in Utilities to your's Google Developers Console project number.
 3. Replace WEB_CLIENT_ID in Utilities to OAuth web client id.
 
If you have any questions, send me an email to:
nejchy_at_gmail_dot_com 