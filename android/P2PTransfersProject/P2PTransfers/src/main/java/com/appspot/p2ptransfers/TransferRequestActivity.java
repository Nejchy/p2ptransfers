package com.appspot.p2ptransfers;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.p2pprevozi.p2ptransfers.P2ptransfers;
import com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesTransferRequestResponseRequestMessage;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;

public class TransferRequestActivity extends ActionBarActivity {

    String _RequestKey;
    String _MeetingPointName;
    int _Rating;
    Context _Context;
    P2ptransfers _Service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transfer_request_main);

        _Context = getApplicationContext();

        Bundle extras = getIntent().getExtras();
        _RequestKey = extras.getString("request_key");
        String email = extras.getString("email");
        _MeetingPointName = extras.getString("meeting_point_name");
        _Rating = extras.getInt("rating", 0);

        GoogleAccountCredential _Credential = GoogleAccountCredential.usingAudience(this, "server:client_id:" + Utilities.WEB_CLIENT_ID);
        _Service = new P2ptransfers(AndroidHttp.newCompatibleTransport(), new GsonFactory(), _Credential);
        _Credential.setSelectedAccountName(email);

        initializeControls();
    }

    private void initializeControls() {
        TextView meetingPlaceName = (TextView) findViewById(R.id.meeting_place_name);
        meetingPlaceName.setText(_MeetingPointName);

        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setRating(_Rating);
        ratingBar.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_locationMonitoringID) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.menu_webViewID) {
            Intent intent = new Intent(this, WebActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick_Decline(View v)
    {
        SendResponse(false);
    }

    public void onClick_Accept(View v)
    {
        SendResponse(true);
    }

    private void SendResponse(boolean response){
        new AsyncTask<Object, Void, String>() {
            @Override
            protected String doInBackground(Object... params) {
                Boolean response = (Boolean)params[0];
                String msg;
                APIAPIMessagesTransferRequestResponseRequestMessage message = new APIAPIMessagesTransferRequestResponseRequestMessage();
                message.setRequestKey(_RequestKey);
                message.setIsApproved(response);
                try {
                    _Service.p2ptransfers().respondToUserRequest(message).execute();
                    msg = "Response sent";
                } catch (IOException e) {
                    msg = "error " + e.getMessage();
                    e.printStackTrace();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(_Context, msg, Toast.LENGTH_LONG).show();
            }
        }.execute(response, null, null);
    }
}
