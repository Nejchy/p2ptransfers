package com.appspot.p2ptransfers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class TransferResponseActivity extends ActionBarActivity {

    String _MeetingPointName;
    String _IsAcceptedText;
    String _VehicleDescription;
    private boolean _IsApproved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_response);

        Bundle extras = getIntent().getExtras();
        _VehicleDescription = extras.getString("vehicle_description");
        _MeetingPointName = extras.getString("meeting_point_name");
        _IsAcceptedText = extras.getString("is_approved_text");
        _IsApproved = extras.getBoolean("is_approved", false);

        initializeControls();
    }

    private void initializeControls() {
        TextView meetingPlaceName = (TextView) findViewById(R.id.meeting_place_name_response);
        meetingPlaceName.setText(_MeetingPointName);

        TextView isAcceptedText = (TextView) findViewById(R.id.request_approved_response);
        isAcceptedText .setText(_IsAcceptedText);

        TextView car_data = (TextView) findViewById(R.id.car_data);
        TextView vehicleTextBox = (TextView) findViewById(R.id.vehicleTextBox);
        if(!_IsApproved){
            car_data.setVisibility(View.INVISIBLE);
            vehicleTextBox.setVisibility(View.INVISIBLE);
        }
        else{
            car_data.setVisibility(View.VISIBLE);
            vehicleTextBox.setVisibility(View.VISIBLE);
            car_data.setText(_VehicleDescription);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_locationMonitoringID) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.menu_webViewID) {
            Intent intent = new Intent(this, WebActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
