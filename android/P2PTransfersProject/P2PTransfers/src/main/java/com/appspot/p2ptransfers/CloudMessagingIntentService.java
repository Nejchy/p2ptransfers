package com.appspot.p2ptransfers;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class CloudMessagingIntentService extends IntentService {

    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    static final String LOG_TAG = "CloudMessagingIntentService";

    public CloudMessagingIntentService(){
        super("CloudMessagingIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString(), null);
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString(), null);
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                if (extras.getString("type", "").equals("PUSH_NOTIFICATION_DRIVER")){
                    Intent notificationIntent = new Intent(this, TransferRequestActivity.class);
                    String request_key = extras.getString("request_key");
                    String meeting_point_name = extras.getString("meeting_point_name");
                    String email = extras.getString("email");
                    int rating = Integer.parseInt(extras.getString("rating", "0"));
                    notificationIntent.putExtra("request_key", request_key);
                    notificationIntent.putExtra("meeting_point_name", meeting_point_name);
                    notificationIntent.putExtra("rating", rating);
                    notificationIntent.putExtra("email", email);
                    sendNotification(extras.getString("message", ""), notificationIntent);
                }
                else if (extras.getString("type", "").equals("PUSH_NOTIFICATION_PASSENGER")){
                    Intent notificationIntent = new Intent(this, TransferResponseActivity.class);
                    String is_approved_text = extras.getString("is_approved_text");
                    String meeting_point_name = extras.getString("meeting_point_name");
                    String vehicle_description = extras.getString("vehicle_description");
                    boolean is_approved = extras.getString("is_approved", "false").equalsIgnoreCase("true") ? true : false;
                    notificationIntent.putExtra("is_approved_text", is_approved_text);
                    notificationIntent.putExtra("meeting_point_name", meeting_point_name);
                    notificationIntent.putExtra("vehicle_description", vehicle_description);
                    notificationIntent.putExtra("is_approved", is_approved);
                    sendNotification(extras.getString("message", ""), notificationIntent);
                }
                else if (extras.getString("type", "").equals("START_MONITORING")){
                    String email = extras.getString("email");
                    Intent startServiceIntent= new Intent(this, GPSLocationService.class);
                    startServiceIntent.putExtra("email", email);
                    startServiceIntent.setAction(GPSLocationService.START_LOCATION_MONITORING);
                    startService(startServiceIntent);
                    sendNotification("Location monitoring started", null);
                }
                else if (extras.getString("type", "").equals("STOP_MONITORING")){
                    Intent startServiceIntent = new Intent(this, GPSLocationService.class);
                    startServiceIntent.setAction(GPSLocationService.STOP_LOCATION_MONITORING);
                    startService(startServiceIntent);
                    sendNotification("Location monitoring stopped", null);
                }
                else{
                    sendNotification("Unrecognized message type: ", null);
                }
                Log.i(LOG_TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        CloudMessagingBroadcastReceiver.completeWakefulIntent(intent);
    }

    private int getUniqueNotificationId(){
        return  (int) (System.currentTimeMillis() & 0xfffffff);
    }

    private void sendNotification(String msg, Intent notificationIntent) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationIntent == null){
            notificationIntent = new Intent(this, MainActivity.class);
        }
        PendingIntent contentIntent = PendingIntent.getActivity(this, getUniqueNotificationId(),notificationIntent, 0);

        builder = new NotificationCompat.Builder(this).setDefaults(Notification.DEFAULT_ALL)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("P2P Transfers")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                        .setContentText(msg);
        builder.setPriority(Notification.PRIORITY_MAX);
        builder.setOnlyAlertOnce(true);
        builder.setAutoCancel(true);
        builder.setContentIntent(contentIntent);
        mNotificationManager.notify(getUniqueNotificationId(), builder.build());
    }
}
