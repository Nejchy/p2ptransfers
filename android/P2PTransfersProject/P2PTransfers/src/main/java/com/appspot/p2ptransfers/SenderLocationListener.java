package com.appspot.p2ptransfers;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import com.appspot.p2pprevozi.p2ptransfers.P2ptransfers;
import com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesLocationDataMessage;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;

public class SenderLocationListener implements LocationListener {
    final static String LOG_TAG = "LocationListener";
    GoogleAccountCredential _Credential;
    P2ptransfers _Service;

    public SenderLocationListener(Context context, String email){
        _Credential = GoogleAccountCredential.usingAudience(context, "server:client_id:" + Utilities.WEB_CLIENT_ID);
        _Service = new P2ptransfers(AndroidHttp.newCompatibleTransport(), new GsonFactory(), _Credential);
        _Credential.setSelectedAccountName(email);
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            APIAPIMessagesLocationDataMessage message = new APIAPIMessagesLocationDataMessage();
            message.setLatitude(latitude);
            message.setLongitude(longitude);

            _Service.p2ptransfers().saveLocationData(message).execute();
            Log.i(LOG_TAG, "Location changed: latitude " + latitude + " longitude " + longitude);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
