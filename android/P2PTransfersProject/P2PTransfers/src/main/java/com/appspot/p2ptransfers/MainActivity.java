package com.appspot.p2ptransfers;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.p2pprevozi.p2ptransfers.P2ptransfers;
import com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesRegistrationIDMessage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;

public class MainActivity extends Activity {

    private static final String LOG_TAG = "Main activity";
    private static final int ACCOUNT_CHOOSER_INTENT = 4321;

    TextView _AccountLabel;
    Button _ToggleLocationMonitoringButton;
    Button _SignInOutButton;

    boolean _SignedIn = false;

    boolean _IsLocationMonitoringEnabled = false;
    GoogleCloudMessaging _Gcm;
    Context _Context;
    P2ptransfers _Service;
    SharedPreferences _Settings;
    GoogleAccountCredential _Credential;
    String _AccountName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeControls();

        _Context = getApplicationContext();
        _Settings = getPreferences();

        _Credential = GoogleAccountCredential.usingAudience(MainActivity.this,"server:client_id:" + Utilities.WEB_CLIENT_ID);
        _Service = new P2ptransfers(AndroidHttp.newCompatibleTransport(), new GsonFactory(), _Credential);

        _SignedIn = checkIfUserSignedIn();
        if (_SignedIn){
            signedIn();
        }
        else{
            signedOut();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        _IsLocationMonitoringEnabled = isGPSServiceRunning();
        toggleLocationMonitoringButton();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ACCOUNT_CHOOSER_INTENT:
                if (data != null && data.getExtras() != null) {
                    String accountName = data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        setAccountName(accountName);
                        setGooglePlayServices();
                        signedIn();
                        Toast.makeText(this, accountName, Toast.LENGTH_LONG).show();
                    }
                }
            break;
        }
    }

    private boolean checkIfUserSignedIn(){
        String savedAccount = _Settings.getString(Utilities.ACCOUNT_ID, "");
        if (savedAccount.isEmpty()){
            return false;
        }

        _Credential.setSelectedAccountName(savedAccount);
        _AccountName = savedAccount;
        return true;
    }

    private void signedIn(){
        _AccountLabel.setText(_AccountName);
        _SignInOutButton.setText("Sign out");
        _ToggleLocationMonitoringButton.setEnabled(true);
    }

    private void signedOut(){
        _AccountLabel.setText("Not signed in");
        _SignInOutButton.setText("Sign in");
        _ToggleLocationMonitoringButton.setEnabled(false);
    }

    private void setAccountName(String accountName) {
        SharedPreferences.Editor editor = _Settings.edit();
        editor.putString(Utilities.ACCOUNT_ID, accountName);
        editor.commit();
        _Credential.setSelectedAccountName(accountName);
        _AccountName = accountName;
        _SignedIn =_Credential.getSelectedAccountName() != null;
    }

    private void toggleLocationMonitoringButton() {
        if (_IsLocationMonitoringEnabled){
            _ToggleLocationMonitoringButton.setText(R.string.stop_location_monitoring_label);
        }
        else{
            _ToggleLocationMonitoringButton.setText(getString(R.string.start_location_monitoring_label));
        }
    }

    private boolean isGPSServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (GPSLocationService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void setGooglePlayServices(){
        if (checkPlayServices()) {
            _Gcm = GoogleCloudMessaging.getInstance(this);

            if (getRegistrationId(_Context).isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(LOG_TAG, "No valid Google Play Services APK found.");
            Toast.makeText(this, "Google play not supported on this device.", Toast.LENGTH_LONG).show();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        Utilities.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(LOG_TAG, "Google play not supported on this device.");
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getPreferences();
        String registrationId = prefs.getString(Utilities.PROPERTY_NAME_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(LOG_TAG, "Registration not found.");
            return "";
        }

        int registeredVersion = prefs.getInt(Utilities.PROPERTY_NAME_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(LOG_TAG, "App version changed.");
            return "";
        }

        String loggedInUser = prefs.getString(Utilities.ACCOUNT_ID, "");
        if (!loggedInUser.equals(_AccountName)) {
            Log.i(LOG_TAG, "Logged in user changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getPreferences() {
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return Integer.MIN_VALUE;
        } catch (NullPointerException e) {
            return Integer.MIN_VALUE;
        }
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (_Gcm == null) {
                        _Gcm = GoogleCloudMessaging.getInstance(_Context);
                    }
                    String registrationID = _Gcm.register(Utilities.PROJECT_NUMBER);
                    msg = "Device registered, registration ID=" + registrationID;
                    APIAPIMessagesRegistrationIDMessage message = new APIAPIMessagesRegistrationIDMessage();
                    message.setRegistrationId(registrationID);
                    _Service.p2ptransfers().saveAndroidID(message).execute();

                   storeRegistrationId(_Context, registrationID);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(_Context, msg, Toast.LENGTH_LONG).show();
            }
        }.execute(null, null, null);
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getPreferences();
        int appVersion = getAppVersion(context);
        Log.i(LOG_TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Utilities.PROPERTY_NAME_REG_ID, regId);
        editor.putInt(Utilities.PROPERTY_NAME_APP_VERSION, appVersion);
        editor.putString(Utilities.ACCOUNT_ID, _AccountName);
        editor.commit();
    }

    private void initializeControls() {
        _ToggleLocationMonitoringButton = (Button) findViewById(R.id.start_stop_location_monitoring);
        _ToggleLocationMonitoringButton.setEnabled(false);

        _SignInOutButton = (Button)findViewById(R.id.btn_sign_in_out);
        _SignInOutButton.setEnabled(true);

        _AccountLabel = (TextView)findViewById(R.id.txtAccount);
        _AccountLabel.setText("Not signed in");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_webViewID) {
            Intent intent = new Intent(this, WebActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick_Location_Monitoring_Button(View v)
    {
        if (!_IsLocationMonitoringEnabled && _SignedIn){
            startLocationMonitoring();
            _IsLocationMonitoringEnabled = true;
        }
        else {
            stopLocationMonitoring();
            _IsLocationMonitoringEnabled = false;
        }
        toggleLocationMonitoringButton();
    }

    public void onClick_Location_SignInOut_Button(View v)
    {
        if (_SignedIn){
            setAccountName(null);
            signedOut();
        }
        else {
            startActivityForResult(_Credential.newChooseAccountIntent(), ACCOUNT_CHOOSER_INTENT);
        }
    }

    private void startLocationMonitoring() {
        Toast.makeText(this, "Location monitoring started", Toast.LENGTH_LONG).show();
        Log.d(LOG_TAG, "startLocationMonitoring method");

        Intent intent = new Intent(this, GPSLocationService.class);
        intent.putExtra("email", _Credential.getSelectedAccountName());
        intent.setAction(GPSLocationService.START_LOCATION_MONITORING);
        startService(intent);
    }

    private void stopLocationMonitoring() {
        Toast.makeText(this, "Location monitoring stopped", Toast.LENGTH_LONG).show();
        Log.d(LOG_TAG, "stopLocationMonitoring method");
        Intent intent = new Intent(this, GPSLocationService.class);
        intent.setAction(GPSLocationService.STOP_LOCATION_MONITORING);

        stopService(intent);
    }
}
