/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://code.google.com/p/google-apis-client-generator/
 * (build: 2013-12-19 23:55:21 UTC)
 * on 2014-01-25 at 14:41:36 UTC 
 * Modify at your own risk.
 */

package com.appspot.p2pprevozi.p2ptransfers;

/**
 * Service definition for P2ptransfers (v1).
 *
 * <p>
 * P2PTransfers API
 * </p>
 *
 * <p>
 * For more information about this service, see the
 * <a href="" target="_blank">API Documentation</a>
 * </p>
 *
 * <p>
 * This service uses {@link P2ptransfersRequestInitializer} to initialize global parameters via its
 * {@link Builder}.
 * </p>
 *
 * @since 1.3
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public class P2ptransfers extends com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient {

  // Note: Leave this static initializer at the top of the file.
  static {
    com.google.api.client.util.Preconditions.checkState(
        com.google.api.client.googleapis.GoogleUtils.MAJOR_VERSION == 1 &&
        com.google.api.client.googleapis.GoogleUtils.MINOR_VERSION >= 15,
        "You are currently running with version %s of google-api-client. " +
        "You need at least version 1.15 of google-api-client to run version " +
        "1.17.0-rc of the p2ptransfers library.", com.google.api.client.googleapis.GoogleUtils.VERSION);
  }

  /**
   * The default encoded root URL of the service. This is determined when the library is generated
   * and normally should not be changed.
   *
   * @since 1.7
   */
  public static final String DEFAULT_ROOT_URL = "https://p2pprevozi.appspot.com/_ah/api/";

  /**
   * The default encoded service path of the service. This is determined when the library is
   * generated and normally should not be changed.
   *
   * @since 1.7
   */
  public static final String DEFAULT_SERVICE_PATH = "p2ptransfers/v1/";

  /**
   * The default encoded base URL of the service. This is determined when the library is generated
   * and normally should not be changed.
   */
  public static final String DEFAULT_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH;

  /**
   * Constructor.
   *
   * <p>
   * Use {@link Builder} if you need to specify any of the optional parameters.
   * </p>
   *
   * @param transport HTTP transport, which should normally be:
   *        <ul>
   *        <li>Google App Engine:
   *        {@code com.google.api.client.extensions.appengine.http.UrlFetchTransport}</li>
   *        <li>Android: {@code newCompatibleTransport} from
   *        {@code com.google.api.client.extensions.android.http.AndroidHttp}</li>
   *        <li>Java: {@link com.google.api.client.googleapis.javanet.GoogleNetHttpTransport#newTrustedTransport()}
   *        </li>
   *        </ul>
   * @param jsonFactory JSON factory, which may be:
   *        <ul>
   *        <li>Jackson: {@code com.google.api.client.json.jackson2.JacksonFactory}</li>
   *        <li>Google GSON: {@code com.google.api.client.json.gson.GsonFactory}</li>
   *        <li>Android Honeycomb or higher:
   *        {@code com.google.api.client.extensions.android.json.AndroidJsonFactory}</li>
   *        </ul>
   * @param httpRequestInitializer HTTP request initializer or {@code null} for none
   * @since 1.7
   */
  public P2ptransfers(com.google.api.client.http.HttpTransport transport, com.google.api.client.json.JsonFactory jsonFactory,
      com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
    this(new Builder(transport, jsonFactory, httpRequestInitializer));
  }

  /**
   * @param builder builder
   */
  P2ptransfers(Builder builder) {
    super(builder);
  }

  @Override
  protected void initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest<?> httpClientRequest) throws java.io.IOException {
    super.initialize(httpClientRequest);
  }

  /**
   * An accessor for creating requests from the Greetings collection.
   *
   * <p>The typical use is:</p>
   * <pre>
   *   {@code P2ptransfers p2ptransfers = new P2ptransfers(...);}
   *   {@code P2ptransfers.Greetings.List request = p2ptransfers.greetings().list(parameters ...)}
   * </pre>
   *
   * @return the resource collection
   */
  public Greetings greetings() {
    return new Greetings();
  }

  /**
   * The "greetings" collection of methods.
   */
  public class Greetings {

    /**
     * Create a request for the method "greetings.listGreeting".
     *
     * This request holds the parameters needed by the p2ptransfers server.  After setting any optional
     * parameters, call the {@link ListGreeting#execute()} method to invoke the remote operation.
     *
     * @return the request
     */
    public ListGreeting listGreeting() throws java.io.IOException {
      ListGreeting result = new ListGreeting();
      initialize(result);
      return result;
    }

    public class ListGreeting extends P2ptransfersRequest<com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesGreetingCollection> {

      private static final String REST_PATH = "hellogreeting";

      /**
       * Create a request for the method "greetings.listGreeting".
       *
       * This request holds the parameters needed by the the p2ptransfers server.  After setting any
       * optional parameters, call the {@link ListGreeting#execute()} method to invoke the remote
       * operation. <p> {@link
       * ListGreeting#initialize(com.google.api.client.googleapis.services.AbstractGoogleClientRequest)}
       * must be called to initialize this instance immediately after invoking the constructor. </p>
       *
       * @since 1.13
       */
      protected ListGreeting() {
        super(P2ptransfers.this, "GET", REST_PATH, null, com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesGreetingCollection.class);
      }

      @Override
      public com.google.api.client.http.HttpResponse executeUsingHead() throws java.io.IOException {
        return super.executeUsingHead();
      }

      @Override
      public com.google.api.client.http.HttpRequest buildHttpRequestUsingHead() throws java.io.IOException {
        return super.buildHttpRequestUsingHead();
      }

      @Override
      public ListGreeting setAlt(java.lang.String alt) {
        return (ListGreeting) super.setAlt(alt);
      }

      @Override
      public ListGreeting setFields(java.lang.String fields) {
        return (ListGreeting) super.setFields(fields);
      }

      @Override
      public ListGreeting setKey(java.lang.String key) {
        return (ListGreeting) super.setKey(key);
      }

      @Override
      public ListGreeting setOauthToken(java.lang.String oauthToken) {
        return (ListGreeting) super.setOauthToken(oauthToken);
      }

      @Override
      public ListGreeting setPrettyPrint(java.lang.Boolean prettyPrint) {
        return (ListGreeting) super.setPrettyPrint(prettyPrint);
      }

      @Override
      public ListGreeting setQuotaUser(java.lang.String quotaUser) {
        return (ListGreeting) super.setQuotaUser(quotaUser);
      }

      @Override
      public ListGreeting setUserIp(java.lang.String userIp) {
        return (ListGreeting) super.setUserIp(userIp);
      }

      @Override
      public ListGreeting set(String parameterName, Object value) {
        return (ListGreeting) super.set(parameterName, value);
      }
    }

  }

  /**
   * An accessor for creating requests from the P2ptransfersOperations collection.
   *
   * <p>The typical use is:</p>
   * <pre>
   *   {@code P2ptransfers p2ptransfers = new P2ptransfers(...);}
   *   {@code P2ptransfers.P2ptransfersOperations.List request = p2ptransfers.p2ptransfers().list(parameters ...)}
   * </pre>
   *
   * @return the resource collection
   */
  public P2ptransfersOperations p2ptransfers() {
    return new P2ptransfersOperations();
  }

  /**
   * The "p2ptransfers" collection of methods.
   */
  public class P2ptransfersOperations {

    /**
     * Create a request for the method "p2ptransfers.RespondToUserRequest".
     *
     * This request holds the parameters needed by the p2ptransfers server.  After setting any optional
     * parameters, call the {@link RespondToUserRequest#execute()} method to invoke the remote
     * operation.
     *
     * @param content the {@link com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesTransferRequestResponseRequestMessage}
     * @return the request
     */
    public RespondToUserRequest respondToUserRequest(com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesTransferRequestResponseRequestMessage content) throws java.io.IOException {
      RespondToUserRequest result = new RespondToUserRequest(content);
      initialize(result);
      return result;
    }

    public class RespondToUserRequest extends P2ptransfersRequest<Void> {

      private static final String REST_PATH = "API/RespondToUserRequest";

      /**
       * Create a request for the method "p2ptransfers.RespondToUserRequest".
       *
       * This request holds the parameters needed by the the p2ptransfers server.  After setting any
       * optional parameters, call the {@link RespondToUserRequest#execute()} method to invoke the
       * remote operation. <p> {@link RespondToUserRequest#initialize(com.google.api.client.googleapis.s
       * ervices.AbstractGoogleClientRequest)} must be called to initialize this instance immediately
       * after invoking the constructor. </p>
       *
       * @param content the {@link com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesTransferRequestResponseRequestMessage}
       * @since 1.13
       */
      protected RespondToUserRequest(com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesTransferRequestResponseRequestMessage content) {
        super(P2ptransfers.this, "POST", REST_PATH, content, Void.class);
      }

      @Override
      public RespondToUserRequest setAlt(java.lang.String alt) {
        return (RespondToUserRequest) super.setAlt(alt);
      }

      @Override
      public RespondToUserRequest setFields(java.lang.String fields) {
        return (RespondToUserRequest) super.setFields(fields);
      }

      @Override
      public RespondToUserRequest setKey(java.lang.String key) {
        return (RespondToUserRequest) super.setKey(key);
      }

      @Override
      public RespondToUserRequest setOauthToken(java.lang.String oauthToken) {
        return (RespondToUserRequest) super.setOauthToken(oauthToken);
      }

      @Override
      public RespondToUserRequest setPrettyPrint(java.lang.Boolean prettyPrint) {
        return (RespondToUserRequest) super.setPrettyPrint(prettyPrint);
      }

      @Override
      public RespondToUserRequest setQuotaUser(java.lang.String quotaUser) {
        return (RespondToUserRequest) super.setQuotaUser(quotaUser);
      }

      @Override
      public RespondToUserRequest setUserIp(java.lang.String userIp) {
        return (RespondToUserRequest) super.setUserIp(userIp);
      }

      @Override
      public RespondToUserRequest set(String parameterName, Object value) {
        return (RespondToUserRequest) super.set(parameterName, value);
      }
    }
    /**
     * Create a request for the method "p2ptransfers.SaveAndroidID".
     *
     * This request holds the parameters needed by the p2ptransfers server.  After setting any optional
     * parameters, call the {@link SaveAndroidID#execute()} method to invoke the remote operation.
     *
     * @param content the {@link com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesRegistrationIDMessage}
     * @return the request
     */
    public SaveAndroidID saveAndroidID(com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesRegistrationIDMessage content) throws java.io.IOException {
      SaveAndroidID result = new SaveAndroidID(content);
      initialize(result);
      return result;
    }

    public class SaveAndroidID extends P2ptransfersRequest<Void> {

      private static final String REST_PATH = "API/SaveAndroidID";

      /**
       * Create a request for the method "p2ptransfers.SaveAndroidID".
       *
       * This request holds the parameters needed by the the p2ptransfers server.  After setting any
       * optional parameters, call the {@link SaveAndroidID#execute()} method to invoke the remote
       * operation. <p> {@link SaveAndroidID#initialize(com.google.api.client.googleapis.services.Abstra
       * ctGoogleClientRequest)} must be called to initialize this instance immediately after invoking
       * the constructor. </p>
       *
       * @param content the {@link com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesRegistrationIDMessage}
       * @since 1.13
       */
      protected SaveAndroidID(com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesRegistrationIDMessage content) {
        super(P2ptransfers.this, "POST", REST_PATH, content, Void.class);
      }

      @Override
      public SaveAndroidID setAlt(java.lang.String alt) {
        return (SaveAndroidID) super.setAlt(alt);
      }

      @Override
      public SaveAndroidID setFields(java.lang.String fields) {
        return (SaveAndroidID) super.setFields(fields);
      }

      @Override
      public SaveAndroidID setKey(java.lang.String key) {
        return (SaveAndroidID) super.setKey(key);
      }

      @Override
      public SaveAndroidID setOauthToken(java.lang.String oauthToken) {
        return (SaveAndroidID) super.setOauthToken(oauthToken);
      }

      @Override
      public SaveAndroidID setPrettyPrint(java.lang.Boolean prettyPrint) {
        return (SaveAndroidID) super.setPrettyPrint(prettyPrint);
      }

      @Override
      public SaveAndroidID setQuotaUser(java.lang.String quotaUser) {
        return (SaveAndroidID) super.setQuotaUser(quotaUser);
      }

      @Override
      public SaveAndroidID setUserIp(java.lang.String userIp) {
        return (SaveAndroidID) super.setUserIp(userIp);
      }

      @Override
      public SaveAndroidID set(String parameterName, Object value) {
        return (SaveAndroidID) super.set(parameterName, value);
      }
    }
    /**
     * Create a request for the method "p2ptransfers.SaveLocationData".
     *
     * This request holds the parameters needed by the p2ptransfers server.  After setting any optional
     * parameters, call the {@link SaveLocationData#execute()} method to invoke the remote operation.
     *
     * @param content the {@link com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesLocationDataMessage}
     * @return the request
     */
    public SaveLocationData saveLocationData(com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesLocationDataMessage content) throws java.io.IOException {
      SaveLocationData result = new SaveLocationData(content);
      initialize(result);
      return result;
    }

    public class SaveLocationData extends P2ptransfersRequest<Void> {

      private static final String REST_PATH = "API/SaveLocationData";

      /**
       * Create a request for the method "p2ptransfers.SaveLocationData".
       *
       * This request holds the parameters needed by the the p2ptransfers server.  After setting any
       * optional parameters, call the {@link SaveLocationData#execute()} method to invoke the remote
       * operation. <p> {@link SaveLocationData#initialize(com.google.api.client.googleapis.services.Abs
       * tractGoogleClientRequest)} must be called to initialize this instance immediately after
       * invoking the constructor. </p>
       *
       * @param content the {@link com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesLocationDataMessage}
       * @since 1.13
       */
      protected SaveLocationData(com.appspot.p2pprevozi.p2ptransfers.model.APIAPIMessagesLocationDataMessage content) {
        super(P2ptransfers.this, "POST", REST_PATH, content, Void.class);
      }

      @Override
      public SaveLocationData setAlt(java.lang.String alt) {
        return (SaveLocationData) super.setAlt(alt);
      }

      @Override
      public SaveLocationData setFields(java.lang.String fields) {
        return (SaveLocationData) super.setFields(fields);
      }

      @Override
      public SaveLocationData setKey(java.lang.String key) {
        return (SaveLocationData) super.setKey(key);
      }

      @Override
      public SaveLocationData setOauthToken(java.lang.String oauthToken) {
        return (SaveLocationData) super.setOauthToken(oauthToken);
      }

      @Override
      public SaveLocationData setPrettyPrint(java.lang.Boolean prettyPrint) {
        return (SaveLocationData) super.setPrettyPrint(prettyPrint);
      }

      @Override
      public SaveLocationData setQuotaUser(java.lang.String quotaUser) {
        return (SaveLocationData) super.setQuotaUser(quotaUser);
      }

      @Override
      public SaveLocationData setUserIp(java.lang.String userIp) {
        return (SaveLocationData) super.setUserIp(userIp);
      }

      @Override
      public SaveLocationData set(String parameterName, Object value) {
        return (SaveLocationData) super.set(parameterName, value);
      }
    }

  }

  /**
   * Builder for {@link P2ptransfers}.
   *
   * <p>
   * Implementation is not thread-safe.
   * </p>
   *
   * @since 1.3.0
   */
  public static final class Builder extends com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder {

    /**
     * Returns an instance of a new builder.
     *
     * @param transport HTTP transport, which should normally be:
     *        <ul>
     *        <li>Google App Engine:
     *        {@code com.google.api.client.extensions.appengine.http.UrlFetchTransport}</li>
     *        <li>Android: {@code newCompatibleTransport} from
     *        {@code com.google.api.client.extensions.android.http.AndroidHttp}</li>
     *        <li>Java: {@link com.google.api.client.googleapis.javanet.GoogleNetHttpTransport#newTrustedTransport()}
     *        </li>
     *        </ul>
     * @param jsonFactory JSON factory, which may be:
     *        <ul>
     *        <li>Jackson: {@code com.google.api.client.json.jackson2.JacksonFactory}</li>
     *        <li>Google GSON: {@code com.google.api.client.json.gson.GsonFactory}</li>
     *        <li>Android Honeycomb or higher:
     *        {@code com.google.api.client.extensions.android.json.AndroidJsonFactory}</li>
     *        </ul>
     * @param httpRequestInitializer HTTP request initializer or {@code null} for none
     * @since 1.7
     */
    public Builder(com.google.api.client.http.HttpTransport transport, com.google.api.client.json.JsonFactory jsonFactory,
        com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
      super(
          transport,
          jsonFactory,
          DEFAULT_ROOT_URL,
          DEFAULT_SERVICE_PATH,
          httpRequestInitializer,
          false);
    }

    /** Builds a new instance of {@link P2ptransfers}. */
    @Override
    public P2ptransfers build() {
      return new P2ptransfers(this);
    }

    @Override
    public Builder setRootUrl(String rootUrl) {
      return (Builder) super.setRootUrl(rootUrl);
    }

    @Override
    public Builder setServicePath(String servicePath) {
      return (Builder) super.setServicePath(servicePath);
    }

    @Override
    public Builder setHttpRequestInitializer(com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
      return (Builder) super.setHttpRequestInitializer(httpRequestInitializer);
    }

    @Override
    public Builder setApplicationName(String applicationName) {
      return (Builder) super.setApplicationName(applicationName);
    }

    @Override
    public Builder setSuppressPatternChecks(boolean suppressPatternChecks) {
      return (Builder) super.setSuppressPatternChecks(suppressPatternChecks);
    }

    @Override
    public Builder setSuppressRequiredParameterChecks(boolean suppressRequiredParameterChecks) {
      return (Builder) super.setSuppressRequiredParameterChecks(suppressRequiredParameterChecks);
    }

    @Override
    public Builder setSuppressAllChecks(boolean suppressAllChecks) {
      return (Builder) super.setSuppressAllChecks(suppressAllChecks);
    }

    /**
     * Set the {@link P2ptransfersRequestInitializer}.
     *
     * @since 1.12
     */
    public Builder setP2ptransfersRequestInitializer(
        P2ptransfersRequestInitializer p2ptransfersRequestInitializer) {
      return (Builder) super.setGoogleClientRequestInitializer(p2ptransfersRequestInitializer);
    }

    @Override
    public Builder setGoogleClientRequestInitializer(
        com.google.api.client.googleapis.services.GoogleClientRequestInitializer googleClientRequestInitializer) {
      return (Builder) super.setGoogleClientRequestInitializer(googleClientRequestInitializer);
    }
  }
}
