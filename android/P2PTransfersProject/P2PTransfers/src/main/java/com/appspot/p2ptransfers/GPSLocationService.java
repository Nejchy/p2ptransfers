package com.appspot.p2ptransfers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class GPSLocationService extends Service implements Handler.Callback {
    final static String LOG_TAG = "GPSLocationService";

    public final static  String START_LOCATION_MONITORING = "START_LOCATION_MONITORING";
    public final static  String STOP_LOCATION_MONITORING = "STOP_LOCATION_MONITORING";
    public final static  String NOTIFICATION = "NOTIFICATION";

    LocationListener _locationListener;
    Looper _looper;
    Handler _handler;
    PowerManager.WakeLock _lock;

    @Override
    public void onCreate() {

        Log.d(LOG_TAG, "on create");
        super.onCreate();
        HandlerThread handlerThread = new HandlerThread("GPSLocationServiceThread");
        handlerThread.start();


        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        _lock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOG_TAG);
        _lock.acquire();
        _looper = handlerThread.getLooper();

        _handler = new Handler(_looper, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "on destroy");
        stopMonitoring();
        if (_looper != null){
            _looper.quit();
        }
        if(_lock != null){
            _lock.release();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "on start command");
        _handler.sendMessage(_handler.obtainMessage(0, intent));

        return START_STICKY_COMPATIBILITY;
    }

    private void startMonitoring(String email) {
        stopMonitoring();

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        _locationListener = new SenderLocationListener(this, email);
        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 1000, _locationListener, _looper);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, _locationListener, _looper);
    }


    private void stopMonitoring() {
        if (_locationListener != null){
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            locationManager.removeUpdates(_locationListener);
            _locationListener = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean handleMessage(Message message) {
        Intent intent = (Intent) message.obj;

        String email = intent.getStringExtra("email");
        String action = intent.getAction();

        if (action.equals(NOTIFICATION)){
            NotificationManager mNotificationManager = (NotificationManager)
                    this.getSystemService(Context.NOTIFICATION_SERVICE);

            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,notificationIntent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this).setDefaults(Notification.DEFAULT_ALL)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("P2P Transfers")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText("test"))
                    .setContentText("test");
            builder.setPriority(Notification.PRIORITY_MAX);
            builder.setOnlyAlertOnce(true);
            builder.setAutoCancel(true);
            builder.setContentIntent(contentIntent);
            mNotificationManager.notify(0, builder.build());
        }
        else if (action.equals(STOP_LOCATION_MONITORING))
        {
            stopMonitoring();
            stopSelf();
        }
        else if (action.equals(START_LOCATION_MONITORING) || email == null || email.isEmpty()){
            startMonitoring(email);
        }

        return true;
    }
}