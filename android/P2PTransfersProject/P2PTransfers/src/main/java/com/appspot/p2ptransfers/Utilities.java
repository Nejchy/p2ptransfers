package com.appspot.p2ptransfers;

public final class Utilities {
    static final String PROJECT_NUMBER = "614226059664";

    /**
     * Constant used for google play services
     */
    static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    /**
     * Constants used for storing in shared preferences
     */
    static final String PROPERTY_NAME_APP_VERSION = "app_Version";

    static final String PROPERTY_NAME_REG_ID = "registration_id";

    static final String ACCOUNT_ID = "account_id";

    /**
     * Constants used Google cloud endpoints credentials
     */
    static final String WEB_CLIENT_ID = "732839601908-89a10do847s016adhf7vgjm4nq9r0bp4.apps.googleusercontent.com";
}
